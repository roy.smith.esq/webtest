"use strict";
const misc_helpers_1 = require("../../../helpers/misc_helpers");
misc_helpers_1.Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['activity'], login: { verification: misc_helpers_1.Helpers.verification.verified } });
describe('Loading /activity', function () {
    it('should have <h1 translate="activity.heading.account" class="ng-scope">Account Activity</h1>', function () {
        misc_helpers_1.Helpers.miscHelpers.pageLoad('/activity', undefined);
        browser.wait(protractor.until.elementLocated(By.tagName('h1')), 11000)
            .then(b => {
            b.getAttribute('translate')
                .then(attr => {
                console.log('attr', attr);
                misc_helpers_1.Helpers.miscHelpers.takeScreenshot('activity');
                expect(attr).toBe('true');
            });
        });
    });
});
