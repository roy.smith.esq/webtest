/// <reference path="./refs.d.ts" />

/*
example from pages/fees
<th id="one">{{'fees.label.currency' | translate}}</th>
<th x-test="same('prop','offsetTop','one')">{{'fees.label.withdrawvia' | translate}}</th>
<th x-test="same('prop','offsetLeft','one')">{{'fees.title.fee' | translate}}</th>

where the first (top) will pass and the second (left) will fail

 */


export module LayoutHelper {
  var fs = require('fs');

  /*
  The Helper that provides inline HTML layout validation
   */
  export class LayoutHelper {

    /**
     * the constructor parses any environment or passed parameters and process, exposes them
     * to the rest of the specs. Thus specs should NOT generally access process.env
     * but should instead look for their equivalents here. This is because it is this constructor
     * that deals with things like defaults and generated values.
     *
     *
     *
     * @method constructor
     * @return {[type]}    [description]
     */
    constructor() {
      // console.log('in layout constructor');
    }

    /**
      * Fetches the element which associated with specific attribute name and it's value.
      *
      * @param String filename used as part of any error message
      */
    runTests(filename:string) {
      // Get the list of x-test elements and test each one
      element.all(by.css('[x-test]'))
        .each((element, index) => {
        this.testElementForLayout(element, index, filename);
      });
      element.all(by.css('[translate]'))
        .each((element, index) => {
        this.testElementForUntranslatedString(element, index, filename);
      });
    }

    /**
      * Called for each x-test element to verify whatever embedded condition exists
      *
      * @param Object element
      * @param number index,
      */
    testElementForLayout(element: protractor.ElementFinder, index, filename:string) {
      element.getAttribute('x-test').then((testMethod) => {
        /// ughh globals
        // this.element = element;
        var elementInfo = "x-test=" + testMethod + " as index " + index+ " in "+filename;
        try {
          let methodCall = 'this.' + testMethod.replace(')', ',element, elementInfo)');
          console.log('m', methodCall)
          eval(methodCall);
        } catch (ex) {
          console.log(ex.toString);
        }
      });
    };


    /**
      * Called for each translate element to verify it's innerhtml is not a translation key
      *
      * @param Object element
      * @param number index,
      */
    testElementForUntranslatedString(element: protractor.ElementFinder, index, filename:string) {
      element.getInnerHtml().then(ihtml => {
        expect(this.isTranslateKey(ihtml)).toBe(false, 'Because '+ihtml+' is an un-translated key in '+filename);
      })
    };

    /**
     * tests a string (inner HTML) to see if it looks like a translate key
     * @param  {string}       text [description]
     * @return {boolean}           [description]
     */
    isTranslateKey(text:string):boolean {
      if (!text)  return false;
      if (new RegExp('[ ,:";\'-]').test(text)) return false;                    // not a key if contains whitespace or punctuation
      if (text.length > 99) return false;                                       // not a key if very long
      if (text.endsWith('.')) return false;                                     // not a key if ends in a full stop
      if (text.indexOf('.') < 0) return false;                                  // not a key of no full stops
      if (text.split('.').length > 1) return true;                              // if after eliminating the above, text is xxxx.xxxx, that's a translate key
      return false;
    }

    /**
      * Runs same to check if the elements have a matching attribute,
      * where the attribute can either be an element property ('prop' eg. offsetTop)
      * or a css Value ('css' eg. color)
      *
      * @param String attributeType either 'prop' or 'css'
      * @param String attribute eg. color or offsetTop
      * @param String someElement the ID of an element to compare to
      * @param String thisElement, the element being tested
      * @param String elementInfo the HTML text used in expect because messages
      */
    same(attributeType, attribute, someElement, thisElement: protractor.ElementFinder, elementInfo: string) {
      console.log('Run Same helper.method %s %s %s ', attributeType, attribute, someElement);
      if (attributeType == 'css') {
        return this.sameCssValue(attribute, someElement, thisElement, elementInfo);
      }
      if (attributeType == 'prop') {
        return this.sameProp(attribute, someElement, thisElement, elementInfo);
      }
    };

    /**
      * Runs same to check if the elements have a matching attribute,
      * where the attribute is a cssValue eg color
      *
      * @param String attribute eg. color
      * @param String someElement the ID of an element to compare to
      * @param String thisElement, the element being tested
      */
    sameCssValue(attribute: string, someElementId: string, thisElement: protractor.ElementFinder, elementInfo: string) {
      try {
        element(by.id(someElementId))
          .getCssValue(attribute)
          .then((someAttribute) => {
          // var some_div_pos_n = parseFloat(some_div_pos);
          console.log('p=' + attribute)
          console.log('sdp=' + someAttribute)
          thisElement.getCssValue(attribute)
            .then((thisAttribute) => {
            console.log('ep=' + thisAttribute)
            expect(thisAttribute).toBe(someAttribute, "Because element whose " + elementInfo + " where " + attribute + " position is not same as " + someElementId);
          });
        },
          (err) => {
            console.log('[lh85] Error ' + err.name + ': ' + err.message + '. Check referenced ID');
          });
      } catch (ex) {
        console.log('ex', ex)
      }
    }


    /**
      * Runs same to check if the elements have a matching attribute,
      * where the attribute is an element propert eg offsetTop
      *
      * @param String attribute eg. offsetTop
      * @param String someElement the ID of an element to compare to
      * @param String thisElement, the element being tested
      */
    sameProp(attribute: string, someElementId: string, thisElement: protractor.ElementFinder, elementInfo: string) {
      try {
        element(by.id(someElementId))
          .getAttribute(attribute)
          .then((someAttribute) => {
          // var some_div_pos_n = parseFloat(some_div_pos);
          // console.log('p=' + attribute)
          console.log('sdp=' + someAttribute)
          thisElement.getAttribute(attribute)
            .then((thisAttribute) => {
            console.log('ep=' + thisAttribute)
            expect(thisAttribute).toBe(someAttribute, "Because element whose " + elementInfo + " where " + attribute + " position is not same as " + someElementId);
          });
        },
          (err) => {
            console.log('[lh85] Error ' + err.name + ': ' + err.message + '. Check referenced ID');
          });
      } catch (ex) {
        console.log('ex', ex)
      }
    }


    /**
      * Handles the test of the container.
      *
      * @param String some_div
      */
    container(containerElementId: string, thisElement: protractor.ElementFinder, elementInfo: string) {
      console.log('Run Container method');
      var containerElement = element(by.id(containerElementId));

      this.check_container_size(containerElement, thisElement, containerElementId, elementInfo);
      this.check_container_overflow('width', containerElement, thisElement, containerElementId, elementInfo);
    }

    check_container_size(containerElement: protractor.ElementFinder, thisElement: protractor.ElementFinder, containerElementId, elementInfo) {
      containerElement
        .getSize()
        .then((containerDimensions) => {
        thisElement.getSize()
          .then((elementDimensions) => {
          expect(elementDimensions.width).toBeLessThan(containerDimensions.width, "Because element "+elementInfo+" whose width is larger than " + containerElementId);
          expect(elementDimensions.height).toBeLessThan(containerDimensions.height, "Because element '+elementInfo+' whose height is larger than " + containerElementId);
        });
      });
    };

    /**
    * Checks if the element is overflowing vertically from it's container.
     * @method check_container_overflow
     * @param  {string}                   axis               [description]
     * @param  {protractor.ElementFinder} containerElement   [description]
     * @param  {protractor.ElementFinder} thisElement        [description]
     * @param  {[type]}                   containerElementId [description]
     * @param  {[type]}                   elementInfo        [description]
     * @return {[type]}                                      [description]
     */
    check_container_overflow(axis: string, containerElement: protractor.ElementFinder, thisElement: protractor.ElementFinder, containerElementId, elementInfo) {

      if (axis == 'height') {
        var offset = 'offsetTop';
      } else {
        var offset = 'offsetLeft';
      }
      containerElement
        .getAttribute(offset)
        .then((some_div_offset_top) => {
        thisElement.getAttribute(offset)
          .then((element_offset) => {

          containerElement.getSize()
            .then((containerDimensions) => {
            thisElement.getSize()
              .then((element_dimensions) => {

              // console.log("element_dimension " + element_dimension.height);
              // console.log("some_div_dimension : " + some_div_dimension.height);
              // console.log("Some div offsetTop : " + some_div_offset_top);
              // console.log("element offsetTop : " + element_offset_top);

              expect((element_dimensions.height + parseFloat(offset))).toBeLessThan(containerDimensions.height, "Because element whose " + elementInfo + " " + axis + " is overflow on " + containerElementId);
            });
          });
        });
      });
    }
  }

  // instantiate and invoke constructor
  export var layoutHelper = new LayoutHelper();
}
