import {Component} from 'angular2/core';

import {ScreenshotOptions} from '../../services/screenshot_options';
import {MATERIAL_DIRECTIVES} from 'ng2-material/all';

@Component({
  selector: 'screenshotgrid',
  templateUrl: './components/screenshotgrid/screenshotgrid.html',
  styleUrls: ['./components/screenshotgrid/screenshotgrid.css'],
  directives: [MATERIAL_DIRECTIVES]
})
export class ScreenshotgridCmp {
  public whitelabelModel: string = '';
  public pageModel: string = '';
  public httpInProgress = false;

  constructor(public options: ScreenshotOptions) {
    if (this.options.getFilenames() && this.options.getFilenames().length > 0) {
      this.loadImages();
    }
  }

  /**
   * iterate filenames and fetch each one
   * @method loadImages
   */
  loadImages() {
    this.options.getFilenames().forEach((fname) => {
      this.fetchImage(fname);
    });
  }

  fetchImage(fname) {
    this.options.fetchImage(fname);
    console.log(fname.filename);
  }
}
