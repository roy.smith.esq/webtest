// express functions used by server.ts to parse the screenshotOptions from the web app into a list of filenames and parse
// and then invoke protractor

/**
 * Runs protractor by requiring launch.js from the installation and then calling its init() method
 * @method runscreenshot
 * @param  {[type]}      req  [description]
 * @param  {[type]}      resp [description]
 * @param  {Function}    next [description]
 * @return {[type]}           [description]
 */
export function runscreenshot(req, resp, next) {
  var filenames = [];                                                           // options unrolled to an array
  console.log('[se14] in server runscreenshot with ', req.body);
  iterateOptions(req.body, filenames);
  console.log('converted to  ', filenames);
  if (!filenames || !filenames.length || filenames.length === 0) {
    resp.status(400).send('Not all options were set.');
    next();
    return;
  }
  resp.json(filenames);
  var pathToConf = '../protractor/conf/';                                       // relative to /html_runner

  var paramsForProtractorSpawn = [];
  paramsForProtractorSpawn.push('--params.filenames=' + JSON.stringify(filenames));
  if (req.body.login && req.body.login.verification && (req.body.login.verification !== 'none')) {   // if a login is required
    // console.log('adding ',req.body.login.verification )
    paramsForProtractorSpawn.push('--params.anx={"login":' + JSON.stringify(req.body.login)+'}');
  }
  // else {
    // var params2 = undefined;
  // }
  var configFile = pathToConf + 'metaspec_screenshotter.js';
  paramsForProtractorSpawn.push(configFile);
  console.log('\nLaunching protractor ...',paramsForProtractorSpawn);

  var pro = require('child_process').spawn('protractor', paramsForProtractorSpawn);
  pro.stdout.on('data', (data) => {
    console.log('stdout: ' + data);
  });
}

/**
 * iterate through the hierarchy of options.
 * upon reaching the bottom level, append to an array of path/screen combinations and delete the output file
 *
 * @method iterateOptions
 * @param  {[type]}       opts the incoming options
 * @param  {[type]}       filenames the outgoing list of filenames
 */
function iterateOptions(opts, filenames) {
  var fs = require('fs');
  var url, filename;
  for (var size of opts.sizes) {
    if (!size.selected) { continue; }
    for (var env of opts.envs) {
      if (!env.selected) continue;
      for (var wl of opts.whitelabels) {
        if (!wl.selected) continue;
        url = env.name + '.' + wl.name;
        for (var page of opts.pages) {
          if (!page.selected) continue;
          url = env.name + '.' + wl.name + page.name;
          filename = url.replace(/\W+/g, '_') + '_' + size.x + 'x' + size.y + '.png';
          // store filename and details to an array
          filenames.push({
            url: url,
            env: env.name,
            wl: wl.name,
            page: page.name,
            size: size,
            filename: filename
          });
          // delete filename
          try {
            var fn = '/screenshots/' + filename;
            console.log(' deleting ' + fn);
            fs.unlinkSync(fn);
          } catch (ex) {
            console.log('exception ' + ex + ' deleting ' + fn);
          }
        }
      }
    }
  }
}
