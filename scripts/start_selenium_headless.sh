#!/bin/bash
#
# sets up headless operation (ie. DISPLAY : 10 and start xvfb)
# then calls start_selenium.sh to run the selenium server on default port (4444)
export DISPLAY=:10
/etc/init.d/xvfb restart
sleep 2
/GITREPO/scripts/start_selenium.sh
