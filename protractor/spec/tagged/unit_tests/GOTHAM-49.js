"use strict";
const misc_helpers_1 = require("../../../helpers/misc_helpers");
misc_helpers_1.Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['GOTHAM-49'], login: { verification: misc_helpers_1.Helpers.verification.unverified } });
describe('Fixed GOTHAM-49, when switching between basic and advanced ', () => {
    browser.ignoreSynchronization = true;
    misc_helpers_1.Helpers.miscHelpers.pageLoad('/user', undefined);
    it('should not display busy.jade ', () => {
        browser.wait(protractor.until.elementLocated(By.id('advanced')), 20000)
            .then(inp => {
            inp.click().then(() => {
                console.log('GOTHAM-49: This spec is impossible to test via protractor because the fault is only visible briefly while JS is executing, but all protractor functions are suspended until JS completes, by which time the page has been refreshed.');
                console.log('GOTHAM-49: Test by observation while the spec is running');
            });
        });
    });
});
