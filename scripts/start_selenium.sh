#!/bin/bash
#
# runs the selenium server on default port (4444)
if [ `id -u` == 0 ]
 then
 echo $0 must be run as a non-root user because Chromium will refuse to run as root for security reasons. Try 'su -c /GITREPO/scripts/start_selenium.sh protractor'.
 exit 1
fi

export JAVA_HOME=/programs/jre1.8.0_66
PATH=$PATH:$JAVA_HOME/bin
nohup webdriver-manager start &
tail -f nohup.out
