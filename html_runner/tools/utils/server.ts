import * as connectLivereload from 'connect-livereload';
import * as express from 'express';
import * as tinylrFn from 'tiny-lr';
import * as openResource from 'open';
import * as serveStatic from 'serve-static';
import * as serveIndex from 'serve-index';
import {resolve} from 'path';
import {APP_BASE, APP_DEST, DOCS_DEST, LIVE_RELOAD_PORT, DOCS_PORT, PORT} from '../config';

let tinylr = tinylrFn();

export function serveSPA() {
  let server = express();
  var bodyParser = require('body-parser');
  server.use(bodyParser.json()); // support json encoded bodies
  server.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
  tinylr.listen(LIVE_RELOAD_PORT);

  // /screenshots serves the contents of the /screenshots directory
  server.use(
    '/screenshots/',
    serveIndex('/screenshots', { 'icons': true }),
    serveStatic('/screenshots', { index: false })
    );


  // /downloads serves the contents of the /downloads directory
  // this is used to serve a tar of the webtest jail
  server.use(
    '/downloads/',
    serveIndex('/downloads', { 'icons': true }),
    serveStatic('/downloads', { index: false })
    );

  // /runscreenshot runs protractor with the screeenshotter test spec
  var runscreenshot = require('./anxe2e/screenshot_express').runscreenshot;
  server.use(
    '/runscreenshot',
    runscreenshot
    );

  server.use(
    APP_BASE,
    connectLivereload({ port: LIVE_RELOAD_PORT }),
    express.static(process.cwd())
    );

  server.listen(PORT, () => {
    openResource('http://localhost:' + PORT + APP_BASE + APP_DEST);
  }
    );
}

export function notifyLiveReload(e) {
  let fileName = e.path;
  tinylr.changed({
    body: { files: [fileName] }
  });
}

export function serveDocs() {
  let server = express();

  server.use(
    APP_BASE,
    serveStatic(resolve(process.cwd(), DOCS_DEST))
    );
  console.info('Static content from ' + resolve(process.cwd(), DOCS_DEST) + ' is being served at ' + DOCS_PORT + APP_BASE + ' ');

  server.listen(DOCS_PORT, () =>
    openResource('http://localhost:' + DOCS_PORT + APP_BASE)
    );

}
