#!/bin/bash
screenshotOptions='{
  "envs": [{ "name": "oat", "selected": true }, { "name": "sit" }],
  "whitelabels": [{ "name": "dcexe.com", "selected": true }, { "name": "anxbtc.com", "selected": true }, { "name": "anxpro.com" }],
  "pages": [{ "name": "/pages/about", "selected": true }, { "name": "/pages/fees", "selected": true }, { "name": "foo" }],
  "sizes": [
    { "name": "laptop", "x": 1140, "y": 900, "selected": false },
    { "name": "iPhone5", "x": 320, "y": 5000, "selected": false },
    { "name": "S4", "x": 1360, "y": 5000, "selected": true },
    { "name": "foo", "x": 320, "y": 5000, "selected": false }
  ]
}'

curl -v -X POST -d "$screenshotOptions" -H 'Content-Type: application/json' http://localhost:5555/runscreenshot
