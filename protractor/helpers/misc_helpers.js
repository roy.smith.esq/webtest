"use strict";
var Helpers;
(function (Helpers) {
    var fs = require('fs');
    Helpers.verification = {
        none: '',
        verified: 'VERIFIED',
        unverified: 'UNVERIFIED',
        preverified: 'PRE_VERIFIED',
    };
    (function (TwofaType) {
        TwofaType[TwofaType["NONE"] = 0] = "NONE";
        TwofaType[TwofaType["SMS"] = 1] = "SMS";
        TwofaType[TwofaType["OTP"] = 2] = "OTP";
    })(Helpers.TwofaType || (Helpers.TwofaType = {}));
    var TwofaType = Helpers.TwofaType;
    class MiscHelpers {
        constructor() {
            this.SS_PATH = '/screenshots/';
            this.SCHEME = 'https://';
            this.params = {};
            this.log('in mh constructor');
            try {
                if (!browser) {
                    return;
                }
            }
            catch (ex) {
                return;
            }
            this.log('command line params anx ', browser.params.anx);
            try {
                browser.params.anx = JSON.parse(browser.params.anx);
            }
            catch (ex) {
                browser.params.anx = {};
            }
            if (browser.params.anx.env) {
                this.params.env = browser.params.anx.env;
            }
            if (browser.params.anx.whitelabel) {
                this.params.whitelabel = browser.params.anx.whitelabel;
            }
            if (browser.params.anx.login) {
                this.params.login = browser.params.anx.login;
            }
            this.params.password = process.env.ANX_PASSWORD;
            this.params.baseUrl = process.env.ANX_URL;
            this.params.width = process.env.ANX_X ? process.env.ANX_X : 1200;
            this.params.height = process.env.ANX_Y ? process.env.ANX_Y : 700;
            this.log('height = ' + this.params.height);
            this.log('baseurl = ' + this.params.baseUrl);
            browser.manage().window().setSize(1 * this.params.width, 1 * this.params.height);
        }
        addMockedUrl(url, httpResponse) {
            browser.executeAsyncScript(() => {
                var url = arguments[arguments.length - 3];
                var httpResponse = arguments[arguments.length - 2];
                var callback = arguments[arguments.length - 1];
                function loadHttpService() {
                    try {
                        window['HttpService'] = window['angular'].element(document.body).injector().get('HttpService');
                    }
                    catch (ex) {
                        console.log('HS load failed. retrying...' + ex);
                        setTimeout(function () { loadHttpService(); }, 1000);
                        return;
                    }
                    window['HttpService']._mm[url] = httpResponse;
                    callback(url + ' mocked');
                }
                loadHttpService();
            }, url, httpResponse)
                .then((status) => {
                console.log('[mh130] cb status ', status);
            });
        }
        isOrientationPortrait() {
            return (1 * this.params.height > 1 * this.params.width);
        }
        setInfiniteHeight() {
            browser.manage().window().setSize(1 * this.params.width, 20000);
        }
        takeScreenshot(filename) {
            browser.takeScreenshot().then((png) => {
                var fname = this.SS_PATH + filename.replace('.png', '') + '.png';
                var stream = fs.createWriteStream(fname);
                stream.write(new Buffer(png, 'base64'));
                stream.end();
                this.log('[ss11] Saving screenshot ' + fname);
            });
        }
        setAnxUnitOptions(module, anxUnitOptions) {
            this.anxUnitOptions = anxUnitOptions;
            if (module) {
                module.exports.anxOptions = anxUnitOptions;
            }
        }
        pageLoad(pathOrUrl, promise, loginDone) {
            this.log('in pageLoad with ax', this.anxUnitOptions);
            if (pathOrUrl.startsWith(this.SCHEME)) {
                var url = pathOrUrl;
            }
            else {
                var url = this.SCHEME + this.params.env + '.' + this.params.whitelabel + pathOrUrl;
            }
            if (pathOrUrl === this.currentPath) {
                this.log(url + ' already requested, nothing to do.');
                return;
            }
            if (!loginDone && !this.objectsAreEqual(this.anxUnitOptions.login, this.currentLoginState)) {
                this.log('login needed', this.anxUnitOptions.login, this.currentLoginState);
                this.doLoginByXHR(url, this.anxUnitOptions, () => { this.pageLoad(pathOrUrl, promise, true); }).then((resp) => {
                    this.log('resp', resp);
                    this.currentLoginState = this.anxUnitOptions.login;
                });
                return;
            }
            this.currentPath = pathOrUrl;
            this.log('getting ' + url);
            url = url.replace('naked.', '');
            browser.get(url).then(() => {
                this.log(url + ' loaded');
            });
        }
        doLoginByXHR(url, anxUnitOptions, cb) {
            var creds = this.getEmailAndPasswordForLogin(anxUnitOptions);
            this.log('[mh219] getting page to allow login post ' + url);
            var adjustedUrl = url.replace('naked.', '');
            var got = browser.get(adjustedUrl);
            got.then(() => {
                var ret = browser.executeAsyncScript(function () {
                    var url = arguments[arguments.length - 3];
                    var creds = arguments[arguments.length - 2];
                    console.log('calling xhr login with ' + url, creds);
                    var callback = arguments[arguments.length - 1];
                    var xhr = new XMLHttpRequest();
                    var json = '{"username":"' + creds.e + '","password":"' + creds.p + '","trusted":true}';
                    var parser = document.createElement('a');
                    parser.href = url;
                    url = parser.protocol + '//' + parser.host;
                    if (parser.port.length > 0) {
                        url += ':' + parser.port;
                    }
                    xhr.open('POST', '/moon/v1/login', true);
                    xhr.setRequestHeader('Content-type', 'application/json');
                    xhr.setRequestHeader('Content-length', '' + json.length);
                    xhr.setRequestHeader('Connection', 'close');
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4) {
                            console.log('xhr4 ' + xhr.status, xhr.responseBody + ' ' + url);
                            if (xhr.status > 299) {
                                callback(xhr.status);
                                alert('done. Login status ' + xhr.status + ' ' + creds.e + ' ' + creds.p + ' ' + url);
                            }
                            callback(xhr.status);
                        }
                    };
                    xhr.send(json);
                }, url, creds);
                ret.then((status) => {
                    this.log('[mh243] login status ' + status);
                    cb();
                });
            });
            return got;
        }
        getEmailAndPasswordForLogin(anxUnitOptions) {
            var e = "roy.smith.esq+_VeRiFiEd@gmail.com";
            var p = 'abcd1234';
            console.log('[mh281]  !!!!  warning - using roy.smith.esq@gmail.com while test accounts are broken !!!!!!');
            e = e.replace('VeRiFiEd', anxUnitOptions.login.verification);
            console.log('e ' + e, anxUnitOptions);
            return { e: e, p: p };
        }
        objectsAreEqual(a, b) {
            if (!a && !b) {
                return true;
            }
            if (a == b) {
                return true;
            }
            try {
                return Object.keys(a).length === Object.keys(b).length &&
                    Object.keys(a).reduce((acc, k) => acc && this.objectsAreEqual(a[k], b[k]), true);
            }
            catch (ex) {
                return false;
            }
        }
        waitForUrlToChangeTo(urlRegex) {
            var currentUrl;
            return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
                currentUrl = url;
            }).then(function waitForUrlToChangeTo() {
                return browser.wait(function waitForUrlToChangeTo() {
                    return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
                        return urlRegex.test(url);
                    });
                });
            });
        }
        log(str, obj1, obj2) {
            console.log(str, obj1 || '', obj2 || '');
        }
    }
    Helpers.MiscHelpers = MiscHelpers;
    Helpers.miscHelpers = new MiscHelpers();
})(Helpers = exports.Helpers || (exports.Helpers = {}));
