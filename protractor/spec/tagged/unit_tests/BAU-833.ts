import {Helpers} from "../../../helpers/misc_helpers";
Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['BAU-833', 'fees'] })
/// <reference path="../../../helpers/refs.d.ts"/>
describe('Fixed BAU-833', () => {
  it('should display a translated string instead of "fees.fundingType.CHEQUE"', (done) => {
    Helpers.miscHelpers.pageLoad('/pages/fees', undefined);

    // browser.setLocation('pages/fees').then(() => {
    browser.sleep(10000)
      .then(inp => {
      browser.executeAsyncScript(() => {
        try {
          var callback = arguments[arguments.length - 1];
          // grab an element (any one will do)
          var e = document.getElementById('t_ft');
          e = [].concat(e)[0];  // deal with multiple ids
          // get its scope
          var s = window['angular'].element(e).scope();
          // set up a cheque fee
          console.log(s.data)
          // alert(s.data.fiatDeposit.USD.BANK_WIRE.feeAbs)
          s.data.fiatDeposit.USD.CHEQUE = s.data.fiatDeposit.USD.BANK_WIRE;
          s.$root.$digest();
          callback('mocked' + JSON.stringify(s.data.fiatDeposit.USD))
        } catch (ex) {
          callback(' exception ' + ex);
        }
      })
        .then((status) => {
        console.log('[BAU-833-22] cb status ', status);
        browser.sleep(1000).then(() => {
          browser.wait(protractor.until.elementLocated(By.tagName('md-content')), 5000)
            .then(content => {
            // ([].concat(content))[0].getInnerHtml((ihtml)=>{
            content.getInnerHtml().then((ihtml) => {
              expect(ihtml.indexOf('fees.fundingType')).toBe(-1);
              done();
            })
          })
        })
      });
    });
  });
});
