import {Helpers} from "../../../helpers/misc_helpers";
Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['funds'], login: { verification: Helpers.verification.verified } })
/// <reference path="../../../helpers/refs.d.ts"/>


describe('Loading /activity', function() {
  it('should have <h1 translate="activity.heading.account" class="ng-scope">Account Activity</h1>', function() {
    Helpers.miscHelpers.pageLoad('/funds', undefined);

    // wait for page to load and check for the h1 element
    browser.wait(protractor.until.elementLocated(By.tagName('h1')), 11000)
      .then(b => {
      b.getAttribute('translate')
        .then(attr => {
        console.log('attr', attr);
        Helpers.miscHelpers.takeScreenshot('funds');
        expect(attr).toBe('true')
      })
    });
  });
});
