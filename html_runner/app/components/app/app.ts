import {Component, ViewEncapsulation} from 'angular2/core';
import {
  RouteConfig,
  ROUTER_DIRECTIVES
} from 'angular2/router';
// import {HTTP_PROVIDERS} from 'angular2/http';
import {MATERIAL_DIRECTIVES} from 'ng2-material/all';

import {HomeCmp} from '../home/home';
import {ScreenshotCmp} from '../screenshot/screenshot';
import {ScreenshotgridCmp} from '../screenshotgrid/screenshotgrid';
import {ScreenshotOptions} from '../../services/screenshot_options';

@Component({
  selector: 'app',
  viewProviders: [ScreenshotOptions],
  templateUrl: './components/app/app.html',
  styleUrls: ['./components/app/app.css'],
  encapsulation: ViewEncapsulation.None,
  directives: [ROUTER_DIRECTIVES, MATERIAL_DIRECTIVES]
})
@RouteConfig([
  { path: '/', component: HomeCmp, as: 'Home' },
  { path: '/screenshot', component: ScreenshotCmp, as: 'Screenshot' },
  { path: '/screenshotgrid', component: ScreenshotgridCmp, as: 'Screenshotgrid' }
])
export class AppCmp {}
