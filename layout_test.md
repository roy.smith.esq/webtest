## This is the Specification of the layout test module of anxtest
## Goal
The goal of Layout Test is to provide the web designer with a means to write layout unit tests **within the HTML**

## Example
Here is a piece of HTML, which is a simplified version of an actual bug within anxstatic.

```
<div id="somediv" style="width:40%; border:1px solid grey">
  <span>Here is some text that should appear within the  parent border.
</div>
```      
On a desktop, this renders fine, but on a mobile in portrait the text overflows the border.

The goal is that when this bug is reported, that the developer assigned to fix it can quickly create a test spec that confirms the bug, and which can subsequently confirm the fix. Such a test spec would then go on to form part of a corpus of test specs that can be rerun constantly to detect regression failures.

So the "test spec" for this bug would be

```
<div id="somediv" style="width:40%; border:1px solid grey">
  <span x-test="container('somediv')" >Here is some text that should appear within the  parent border.
</div>
```      


All that has happened is the developer has added `x-test="container('somediv')"`to the span, where:-
* `x-test` is the custom attribute that is used to declare a test spec
* `container` is the name of the test that is to be applied
* `(somediv)` is an argument passed to the container test

So `container` is a predefined Jasmine test that will fetch the top, bottom, left, and right values of somediv, and compare them to the top, bottom, left, and right values of the span, and expect the values to be such that the span is wholly contained within somediv.

## What needs to be written

A helper class (see ???? for an example of template) that :-
1. fetches all elements in the current page that have an x-test attribute. For each ...
1. parses the value of the x-test attribute to determine what test needs to be applied.
1. Calls the appropriate test function with any provided arguments.
1. A set of test functions (listed below). Each function is responsible for fetching any needed values from the browser under test and wrapping them in an `it("Should xxx")` where xxx is an appropriate string to identify the test function and the specific element being tested.`

## Test functions

This list will keep growing as new bugs are found that require them, and also on input from developers.

#### container(parent_element_id)
Checks that the current element is wholly contained within the `parent` element.

#### same(property, element_id)
Checks that the current element has the same CSS property value as the provided element. For example,, same('top', 'sibling') would test that two elements are vertically aligned.
