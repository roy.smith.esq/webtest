Create a protractor test runner to dynamically create the conf.js file based on command line args
I want a single command that will run everything for a single test suite. So no webdriver-manager start or protractor conf/foo.js, instead I want a single command anxtest that does everything (see below for man page).
I don't want multiple conf files because they are a pain to maintain. Instead I want a small number of template conf files, and I want anxtest to create a temporary conf file based on its command line arguments.
I want all spec tests to be written in TypeScript, so no JS examples please. Please follow my examples. This is because TypeScript will auto complete the Jasmine methods which makes it much quicker to write them. We could even use TypeScripts class inheritance to make a base class, that test specs extend.
The first line of each test class should start with a set of tags. See my example.
anxtest man page

### NAME
       anxtest - run a suite of tests against the anxstatic web site
### SYNOPSIS
       anxtest [OPTION]...

The full set of options (ie. $*) is stored into ANXTEST_OPTIONS so it's available for the spec files for parsing and processing.

### DESCRIPTION
Run a single suite of test specs, including  mounting the repo into the chroot jail and starting and stopping the webdriver-manager.
Mandatory arguments to long options are mandatory for short options too.

###### -t tags

list of comma separated tags or filenames. All test specs that contain a matching tag in the first line will be run in the order specified by the command line.
For example  -t login.js,BAU-123 will run the login.js spec file, followed by all files containing a BAU-123 tag

###### -b browsers

list of comma separated browsers
For example  -b chrome,phantomjs

###### -h
Run headless

###### -c base protractor config file *
The pathname relative to the root of the GIT Repo of the anxstatic website of a base protractor conf file. This file is used to create a temporary config file, using the values of the -t, -b, -w arguments.
For example  -c /protactor/conf/master_conf_file.js

###### -C protractor config file
The pathname relative to the root of the GIT Repo of the anxstatic website of a protractor conf file. If this is specified, then -t, -b, -w are ignored.
For example  -c /protactor/conf/my_conf_file.js

###### -j jail *
The full pathname of the downloaded protractor jail on the local machine. This is used by a mount--bind command to mount the repo to the chroot jail. This value can also be provided by exporting PROTRACTOR_JAIL.
For example  -j /home/me/jails/protractor-jail

###### -r repo *
The full pathname of the GIT Repo of the webtest on the local machine. This is used by a mount--bind command to mount the repo to the chroot jail. This value can also be provided by exporting PROTRACTOR_REPO.
For example  -r /mygitrepos/anxstatic

###### -s
Enable screenshots. If this flag is not specified, and calls to "takeScreenshot" will be ignored.

###### -S
Run the tests on Sauce Labs using credentials provided in the environment

######  -u baseUrls
list of comma separated baseurls
For example  -u oat.anxbtc.com,sit.dcexe.com

###### -X width of screen in pixels

###### -Y height of screen in pixels

### ENVIRONMENT
       export SAUCE_USERNAME=YOUR_USERNAME
      export SAUCE_ACCESS_KEY=YOUR_ACCESS_KEY
