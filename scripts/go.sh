O
#!/bin/bash
# topmost runner. conditionally mounts repo and proc, then calls chroot prosession.sh for the lower level runner
# Env variables :-
# export WEBTEST_JAIL=~/jails/protractor-jail
# export WEBTEST_REPO=$PREPO
# PATH=$PATH:$WEBTEST_REPO/scripts

# some temp stuff for now
export ANX_PASSWORD="abcd1234"
export ANX_EMAIL_U="roy.smith.esq+anx_unverified@gmail.com"
export ANX_EMAIL_V="roy.smith.esq+dcexe_verified@gmail.com"
export ANX_URL="https://dev.anxpro.com"
export ANX_X="375"
export ANX_Y="5000"
#export ANX_Y="5527"

export ANX_EMAIL="roy.smith.esq+dcexe_unverified@gmail.com"
export ANX_URL="https://oat.dcexe.com"



if [ -z "$WEBTEST_JAIL" ]
then
  echo You must 'export WEBTEST_JAIL=/the/path/of/your/protractor_jail'. Exiting
  exit 1
fi
if [ -z "$WEBTEST_REPO" ]
then
  echo You must 'export WEBTEST_REPO=/the/path/of/your/anx/repo'. Exiting
  exit 1
fi
if [ -z "$1" ]
then
  echo You must provide the name of the conf.js file on the command line. Exiting
  exit 1
fi
# clear out tmp
rm -rf $WEBTEST_JAIL/tmp/*


# mount repo and proc
mount | grep $WEBTEST_JAIL/GITREPO || sudo mount --bind $WEBTEST_REPO  $WEBTEST_JAIL/GITREPO
mount | grep $WEBTEST_JAIL/proc || sudo mount --bind  /proc $WEBTEST_JAIL/proc
#mount | grep $WEBTEST_JAIL/dev || sudo mount --bind  /dev $WEBTEST_JAIL/dev


set -x
HOME=/home/protractor $WEBTEST_JAIL/usr/sbin/chroot --userspec=protractor:0 $WEBTEST_JAIL /GITREPO/scripts/prosession.sh $*


# unmount
# commented out as it's probably a better UX to prompt once for the sudo and then leave the mounts so subsequent runs bypass the mounts
#sudo umount  $JAIL/GITREPO
#sudo umount  $JAIL/proc
