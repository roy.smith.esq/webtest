/// <reference path="./refs.d.ts"/>
import {Helpers} from "./misc_helpers";
// Its purpose is to provide a dummy describe function so module compilation doesn't fail when each spec is required during the
// discovery pass
declare var GLOBAL;
GLOBAL.describe = function(description: string, specDefinitions: () => void) { return };
import {statSync, readdirSync} from "fs";

var INITIALDIR = '/spec/tagged';
var PROTRACTOR_DIR = 'protractor/';
var INITIALDIR_FROM_ROOT = PROTRACTOR_DIR + INITIALDIR;
var INITIALDIR_FROM_THIS = '..' + INITIALDIR;
var CONFIG_FILE = PROTRACTOR_DIR+'conf/tagged_specs.js';

var argTags: string[] = [];                                                // parsed tags from the command line
var paramsArg: string;                                                   // the raw params from the commnand line, used when invoking protractor
// matching spec files are held in an array within an object per login type. These objects are held both in a map and an array.
// The reason for this apparent complexity is to group specs by lgin type to minimise logging in and out
var matchingSpecFilesMap: { [key: string]: SpecFile } = {};               // map keyed by login type
var matchingSpecFilesArray: SpecFile[] = [];                             // array

interface SpecFile {
  login: string,
  specFiles: string[]
}


/**
 * Process command line args
 * @param  {string[]}  argv [description]
 */
function processArgs(argv: string[]): void {
  argv.forEach((arg) => {
    processArg(arg);
  })
}
/**
 * process a single arg, --param and --tags are the only supported args
 * @method processArg
 * @param  {string}   arg [description]
 */
function processArg(arg: string): void {
  if (!arg.startsWith('--')) {
    return;
  }
  if (arg.startsWith('--tags')) {
    var ts = arg.split('=')[1];
    argTags = ts.split(',');
  }
  if (arg.startsWith('--params')) {
    paramsArg = arg;
  }
}

/**
 * recursively descend the directories under INITIALDIR looking for spec files
 * @method recurseDirectory
 * @param  {string}         dirname [description]
 */
function recurseDirectory(dirname: string): void {
  console.log('Processing dir ', dirname);
  readdirSync(dirname).forEach((name) => {
    let fullname = dirname + '/' + name;
    if (statSync(fullname).isDirectory()) {
      return recurseDirectory(fullname);
    } else {
      processSpecFile(fullname);
    }
  })
}

/**
 * Processes a single spec file by requiring it, then handling its anxOptions, notably tags
 * @method processSpecFile
 * @param  {string}        fullname [description]
 */
function processSpecFile(fullname: string): void {
  if (fullname.indexOf('.js') < 1) {
    return;
  }
  // NB dirname is relative to node cwd but require is relative to this file
  // console.log('Processing file ', fullname)
  var fullnameRelativeToThis = fullname.replace(INITIALDIR_FROM_ROOT, INITIALDIR_FROM_THIS);
  // console.log('converted to ', fullnameRelativeToThis)
  try {
    var spec = require(fullnameRelativeToThis);
    // console.log('discovered spec', spec)
    if (!spec.anxOptions.login) { fullname                                      // default to no login
      spec.anxOptions.login = {verification: Helpers.verification.none};
    }
    if (spec.anxOptions && tagMatch(argTags, spec.anxOptions.tags)) {
      addMatchingSpecFile(fullname, spec.anxOptions.login.verification);
    }
  } catch (ex) {
    console.log('[te41] Exception scanning specfile ' + fullnameRelativeToThis, ex);
  }
}

/**
 * scans an array of tags from the commnad line and an array of tags for the current spec file.
 * Returns true if a match is found
 * @param  {string[]} argTags  [description]
 * @param  {string[]} specTags [description]
 * @return {boolean}           [description]
 */
function tagMatch(argTags: string[], specTags: string[]): boolean {
  for (var argTag of argTags) {
    for (var specTag of specTags) {
      if (argTag == specTag) {
        return true;
      }
    }
  }
  return false;
}

/**
 * once a tag match is found, store the filename in an array of filenames for a given login
 * configuration. Currently a "login configuration" is defined as  verification state, but
 * could be extended to include additional states
 *
 * @method addMatchingSpecFile
 * @param  {string}            fullnameRelativeToThis [description]
 * @param  {string}            login                  [description]
 */
function addMatchingSpecFile(fullnameRelativeToThis: string, login: string): void {
  let specFile: SpecFile;
  if (!matchingSpecFilesMap[login]) {
    specFile = { login: login, specFiles: [] };
    matchingSpecFilesMap[login] = specFile;
    matchingSpecFilesArray.push(specFile);
  } else {
    specFile = matchingSpecFilesMap[login];
  }
  specFile.specFiles.push(fullnameRelativeToThis);
  console.log('!!!', matchingSpecFilesMap, matchingSpecFilesArray);
}

function runProtractor(specFiles: SpecFile[]): void {
  var specArg = '--specs=';
  for (var specFile of specFiles) {
    for (var filename of specFile.specFiles) {
      specArg += ',' + filename;
    }
  }
  specArg = specArg.replace('=,','=');
  var cmd='protractor '+specArg+' \''+paramsArg+'\' '+CONFIG_FILE;
  console.log(cmd);
  var pro = require('child_process').spawn('protractor', [paramsArg, specArg, CONFIG_FILE]);
  pro.stdout.on('data', (data) => {
    console.log('stdout: ' + data);
  });
}


// ------------------
//      main
// -------------------
processArgs(process.argv);
// assumes being run from root of repo
recurseDirectory(INITIALDIR_FROM_ROOT);
// assemble the accumulated list of spec files into a protractor command line
runProtractor(matchingSpecFilesArray);
