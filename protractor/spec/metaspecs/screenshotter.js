"use strict";
const misc_helpers_1 = require("../../helpers/misc_helpers");
const layout_helper_1 = require("../../helpers/layout_helper");
describe('All urls', () => {
    var filenamesString = browser.params.filenames;
    console.log('[ster14] in screenshotter.ts ', filenamesString);
    if (!filenamesString) {
        console.warn('[ster17] Missing filenames - aborting');
        return;
    }
    var filenames = JSON.parse(filenamesString);
    var paramsAnxString = browser.params.anx;
    console.log('[ster23] in screenshotter.ts ', paramsAnxString);
    if (paramsAnxString) {
        try {
            var paramsAnx = JSON.parse(paramsAnxString);
        }
        catch (ex) {
            paramsAnx = paramsAnxString;
        }
        misc_helpers_1.Helpers.miscHelpers.setAnxUnitOptions(undefined, paramsAnx);
    }
    it('should load all pages, produce screenshots and validate inline x-test specs and untranslated strings"', (done) => {
        recurseFilenames(null, null, null, null, null, 0, filenames);
        done();
    });
});
function recurseFilenames(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames) {
    var delay = 5000;
    browser.sleep(delay).then(() => {
        var current = filenames[index];
        if (lastX != current.size.x || lastY != current.size.y) {
            console.log('setting ' + current.size.x + 'x' + current.size.y + ' <> ' + lastX + 'x' + lastY);
            browser.manage().window().setSize(current.size.x, current.size.y).then(() => {
                doPage(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames, current, delay);
            });
        }
        else {
            doPage(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames, current, delay);
        }
    });
}
function doPage(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames, current, delay) {
    if (lastEnv != current.env || lastWl != current.wl) {
        misc_helpers_1.Helpers.miscHelpers.pageLoad(misc_helpers_1.Helpers.miscHelpers.SCHEME + current.url, undefined);
    }
    browser.sleep(delay / 2).then(() => {
        if (lastPage != current.page) {
            console.log('setlocation to ' + current.page);
            browser.setLocation(current.page).then(() => {
                browser.sleep(delay).then(() => {
                    layout_helper_1.LayoutHelper.layoutHelper.runTests(current.filename);
                    misc_helpers_1.Helpers.miscHelpers.takeScreenshot(current.filename);
                });
                if (++index < filenames.length) {
                    recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames);
                }
                return;
            });
        }
        else {
            browser.sleep(delay / 2).then(() => {
                misc_helpers_1.Helpers.miscHelpers.takeScreenshot(current.filename);
            });
            if (++index < filenames.length) {
                recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames);
            }
            return;
        }
    });
}
