#!/bin/bash
# this is the script that is executed by the chroot command
#
# 1. run webdriver-manager in background
# 2. run protractor
# 3. kill webdriver-manager

 set -x
cd /tmp                             # so nohup.out gets purged
mkdir /screenshots 2> /dev/null     # confirm screenshots dire exists
chmod 777 /screenshots              # and is permissive
export JAVA_HOME=/programs/jre1.8.0_66
PATH=$PATH:$JAVA_HOME/bin
nohup webdriver-manager start &
sleep 3
cd /GITREPO/protractor
#protractor conf/master.js $*
protractor conf/$*

# kill everything
# set -x
ps | grep chromedriver | cut -f2 -d\  | xargs kill 2> /dev/null
ps | grep chromium | cut -f2 -d\  | xargs kill 2> /dev/null
ps | grep chrome-sandbox | cut -f2 -d\  | xargs kill 2> /dev/null
ps | grep java | cut -f2 -d\  | xargs kill 2> /dev/null
