import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {MATERIAL_DIRECTIVES} from 'ng2-material/all';
import {Size} from '../../services/screenshot_options';
import {ScreenshotOptions} from '../../services/screenshot_options';

@Component({
  selector: 'screenshot',
  templateUrl: './components/screenshot/screenshot.html',
  styleUrls: ['./components/screenshot/screenshot.css'],
  directives: [MATERIAL_DIRECTIVES]
})
export class ScreenshotCmp {
  public nameModel: string = '';
  public envModel: string = '';
  public login = { verification: 'VERIFIED' };
  public whitelabelModel: string = '';
  public pageModel: string = '';
  public xModel: number;
  public yModel: number;
  public httpInProgress = false;

  constructor(public options: ScreenshotOptions, public router: Router) {
  }

  /**
   * Add a new environment
   * @returns return false to prevent default form submit behavior to refresh the page.
   */
  addEnv(): boolean {
    this.options.addEnv(this.envModel);
    this.envModel = '';
    return false;
  }

  setEnv(name: string) {
    // console.log('setting ' + name)
    this.options.setEnv(name);
    this.options.selectedEnv = name;
  }

  setLoginVerification(verification: string) {
    // console.log('setting ' ,verification)
    this.options.setLogin({ verification: verification });
    this.options.selectedVerification = verification;
  }

  setSize(size: Size) {
    // console.log('setting ' + size.name)
    this.options.setSize(size.name, size.x, size.y);
    this.options.selectedSize = size.name;
  }
  /**
 * Add a new whitelabel
 * @returns return false to prevent default form submit behavior to refresh the page.
 */
  addWhitelabel(): boolean {
    this.options.addWhitelabel(this.whitelabelModel);
    this.whitelabelModel = '';
    return false;
  }  /**
   * Add a new page
   * @returns return false to prevent default form submit behavior to refresh the page.
   */
  addPage(): boolean {
    this.options.addPage(this.pageModel);
    this.pageModel = '';
    return false;
  }
  /**
   * Add a new screen size
   * @returns return false to prevent default form submit behavior to refresh the page.
   */
  addSize(): boolean {
    this.options.addSize(this.nameModel, this.xModel, this.yModel);
    this.nameModel = '';
    return false;
  }

  /**
   * runs the protractor tests using the collected options by calling service which in turn calls runscreenshot endpoint
   * @method run
   */
  run(): void {
    console.log('running...');
    this.httpInProgress = true;
    this.options.runScreenshot(this.options.screenshotOptions, (resp) => {
      this.httpInProgress = false;
      console.log('resp', resp);
      // navigate to results page
      this.router.navigate(['/Screenshotgrid']);
    });
  }
}
