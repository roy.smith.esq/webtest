import {Helpers} from "../../../helpers/misc_helpers";
Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['BAU-597', 'registration'] })
/// <reference path="../../../helpers/refs.d.ts"/>
describe('Fixed BAU-597', () => {
  it('should display a translated string instead of "altcoinx.RegisterCommand.password.password.strength"', (done) => {
    // start by loading a page which is guaranteed to instantiate HttpService
    Helpers.miscHelpers.pageLoad('/register', undefined);
    // then set up a mock response so when we load the password reset page, DUMMY is accepted as the registration code
    Helpers.miscHelpers.addMockedUrl(
      '/moon/v1/user/verifyRegistration/DUMMY',
      { data: { "hasAcceptedTermsConditions": true, "countrySelectRequired": false, "country": "phl", "passwordChangeRequired": false, "language": "en_US" }, status: 200 }
      )

    // now that the HttpService has been mocked, load the password reset page
    browser.setLocation('register/resetPassword/DUMMY').then(() => {
      browser.wait(protractor.until.elementLocated(By.id('passwordId')), 5000)
        .then(inp => {
        // enter the two mismatched passwords
        inp.sendKeys('1234ABCD');
        element(by.id('password2Id')).sendKeys('abcd1234');
        element(by.id('reset_password')).click();
      });

      // wait for the error message to be shown
      browser.wait(protractor.until.elementLocated(By.id('server_error')), 5000)
        .then(span => {
        // and check its contents
        expect(span.getInnerHtml()).toMatch(/....*/);               // regex at least 4 chars
        expect(span.getInnerHtml()).not.toMatch('altcoinx');        // and not the translate key
        done();
      });
    });
    // browser.sleep(3000)
  });
});
