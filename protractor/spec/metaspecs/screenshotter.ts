/*
 * Uses the parameters in a filenames array to cycle through screens and screensize taking screenshots.
 * Intended to be called from the html_runner node service, more specifically the server.ts Express module
 * which is in turn called via the screenshot endpoint, which in turn is called by the ng2 screenshot_options.ts service from the
 * /runscreenshot route
 */

/// <reference path="../../helpers/refs.d.ts"/>

import {Helpers} from "../../helpers/misc_helpers";
import {LayoutHelper} from "../../helpers/layout_helper";

describe('All urls', () => {
  var filenamesString: string = browser.params.filenames;
  console.log('[ster14] in screenshotter.ts ', filenamesString);
  if (!filenamesString) {
    console.warn('[ster17] Missing filenames - aborting');
    return;
  }
  var filenames = JSON.parse(filenamesString);

  var paramsAnxString: string = browser.params.anx;
  console.log('[ster23] in screenshotter.ts ', paramsAnxString);
  if (paramsAnxString) {
    try {
      var paramsAnx = JSON.parse(paramsAnxString);
    } catch (ex) {  // in case it's already an object
      paramsAnx = paramsAnxString;
    }
    Helpers.miscHelpers.setAnxUnitOptions(undefined, paramsAnx);
  }

  // process the array, ie actually fetch and shoot each page. The spec lives at this top level and surrounds all pages.
  // This is because if it was any lower, itr would become async and thus invisible to protractor.
  // Be aware that because of this, jasminejasmineNodeOpts.defaultTimeoutInterval must be set to a suitably high number
  it('should load all pages, produce screenshots and validate inline x-test specs and untranslated strings"', (done) => {
    recurseFilenames(null, null, null, null, null, 0, filenames);
    done();
  });
});

/**
 * recursively called function to process each element of the filename array,
 * ie. load and shoot each page
 *
 * @param  {[type]}         lastEnv   [description]
 * @param  {[type]}         lastWl    [description]
 * @param  {[type]}         lastPage  [description]
 * @param  {[type]}         lastX     [description]
 * @param  {[type]}         lastY     [description]
 * @param  {[type]}         index     [description]
 * @param  {[type]}         filenames [description]
 */
function recurseFilenames(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames) {
  var delay = 5000;
  browser.sleep(delay).then(() => {
    var current = filenames[index];
    if (lastX != current.size.x || lastY != current.size.y) {
      console.log('setting ' + current.size.x + 'x' + current.size.y + ' <> ' + lastX + 'x' + lastY);
      browser.manage().window().setSize(current.size.x, current.size.y).then(() => {
        doPage(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames, current, delay)
        // if (lastEnv != current.lastEnv || lastWl != current.wl) {
        //   Helpers.miscHelpers.pageLoad(Helpers.miscHelpers.SCHEME + current.url, undefined);
        //   // browser.get('https://' + current.url).then(() => {
        //   browser.sleep(delay).then(() => {
        //     if (lastPage != current.page) {
        //       console.log('navigating  to https://' + current.url);
        //       browser.navigate().to('https://' + current.url).then(() => {
        //         // browser.setLocation(current.page).then(() => {
        //         browser.sleep(delay).then(() => {
        //           LayoutHelper.layoutHelper.runTests(current.filename);
        //           Helpers.miscHelpers.takeScreenshot(current.filename);
        //         })
        //         if (++index < filenames.length) {
        //           recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames)
        //         }
        //         return;
        //       })
        //     } else {
        //       browser.sleep(delay).then(() => {
        //         Helpers.miscHelpers.takeScreenshot(current.filename);
        //       })
        //       if (++index < filenames.length) {
        //         recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames)
        //       }
        //       return;
        //     }
        //   })
        // })
        // }
      });
    } else {
      doPage(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames, current, delay)
      // if (lastEnv != current.lastEnv || lastWl != current.wl) {
      //   Helpers.miscHelpers.pageLoad(Helpers.miscHelpers.SCHEME + current.url, undefined);
      //   // browser.get('https://' + current.url).then(() => {
      //   browser.sleep(delay).then(() => {
      //     if (lastPage != current.page) {
      //       console.log('1navigating  to https://' + current.url);
      //       browser.navigate().to('https://' + current.url).then(() => {
      //         // browser.setLocation(current.page).then(() => {
      //         browser.sleep(delay).then(() => {
      //           // LayoutHelper.layoutHelper.runTests(current.filename);
      //           Helpers.miscHelpers.takeScreenshot(current.filename);
      //         })
      //         if (++index < filenames.length) {
      //           recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames)
      //         }
      //         return;
      //       })
      //     } else {
      //       browser.sleep(delay).then(() => {
      //         Helpers.miscHelpers.takeScreenshot(current.filename);
      //       })
      //       if (++index < filenames.length) {
      //         recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames)
      //       }
      //       return;
      //     }
      //   })
      //   // })
      // }
    }
  })
}

function doPage(lastEnv, lastWl, lastPage, lastX, lastY, index, filenames, current, delay) {
  if (lastEnv != current.env || lastWl != current.wl) {
    Helpers.miscHelpers.pageLoad(Helpers.miscHelpers.SCHEME + current.url, undefined);
  }

  // browser.get('https://' + current.url).then(() =>
  browser.sleep(delay/2).then(() => {
    if (lastPage != current.page) {
      // console.log('navigating  to https://' + current.url);
      // browser.navigate().to('https://' + current.url).then(() =>
      console.log('setlocation to ' + current.page);
      browser.setLocation(current.page).then(() => {
        browser.sleep(delay).then(() => {
          LayoutHelper.layoutHelper.runTests(current.filename);
          Helpers.miscHelpers.takeScreenshot(current.filename);
        })
        if (++index < filenames.length) {
          recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames)
        }
        return;
      })
    } else {
      browser.sleep(delay/2).then(() => {
        Helpers.miscHelpers.takeScreenshot(current.filename);
      })
      if (++index < filenames.length) {
        recurseFilenames(current.env, current.wl, current.page, current.size.x, current.size.y, index, filenames)
      }
      return;
    }
  })
  // )
}




// end
//
//  This is the version that processes screensize last on the assumption I could display a page and then change its size
// function ZrecurseFilenamesZ(lastEnv, lastWl, lastPage, index, filenames) {
//   var delay = 5000;
//   browser.sleep(delay).then(() => {
//     var current = filenames[index];
//     console.log(index, current);
//     if (lastEnv != current.lastEnv || lastWl != current.wl) {
//       browser.get('https://' + current.url).then(() => {
//         browser.sleep(delay).then(() => {
//           if (lastPage != current.page) {
//             browser.setLocation(current.page).then(() => {
//               browser.sleep(delay).then(() => {
//                 console.log('setting ' + current.size.x + 'x' + current.size.y);
//                 browser.manage().window().setSize(current.size.x, current.size.y).then(() => {
//                   browser.sleep(delay).then(() => {
//                     Helpers.miscHelpers.takeScreenshot(current.filename);
//                   })
//                   if (++index < filenames.length) {
//                     recurseFilenames(current.env, current.wl, current.page, index, filenames)
//                   }
//                   return;
//                 })
//               })
//             })
//           } else {
//             console.log('setting ' + current.size.x + 'x' + current.size.y);
//             browser.manage().window().setSize(current.size.x, current.size.y).then(() => {
//               browser.sleep(delay).then(() => {
//                 Helpers.miscHelpers.takeScreenshot(current.filename);
//               })
//               if (++index < filenames.length) {
//                 recurseFilenames(current.env, current.wl, current.page, index, filenames)
//               }
//               return;
//             })
//           }
//         })
//       })
//     } else {
//       if (lastPage != current.page) {
//         browser.setLocation(current.page).then(() => {
//           browser.sleep(delay).then(() => {
//             console.log('setting ' + current.size.x + 'x' + current.size.y);
//             browser.manage().window().setSize(current.size.x, current.size.y).then(() => {
//               browser.sleep(delay).then(() => {
//                 Helpers.miscHelpers.takeScreenshot(current.filename);
//               })
//               if (++index < filenames.length) {
//                 recurseFilenames(current.env, current.wl, current.page, index, filenames)
//               }
//               return;
//             })
//           })
//         })
//       } else {
//         console.log('setting ' + current.size.x + 'x' + current.size.y);
//         browser.manage().window().setSize(current.size.x, current.size.y).then(() => {
//           browser.sleep(delay).then(() => {
//             Helpers.miscHelpers.takeScreenshot(current.filename);
//           })
//           if (++index < filenames.length) {
//             recurseFilenames(current.env, current.wl, current.page, index, filenames)
//           }
//           return;
//         })
//       }
//     }
//   })
// }
