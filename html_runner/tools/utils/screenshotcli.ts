// simple test node hack to allow me to call protractor from a commnand line
var pathToConf = '../protractor/conf/';  // relative to /html_runner
// var cli = require('/usr/lib/node_modules/protractor/lib/cli.js');
var launcher = require('/usr/lib/node_modules/protractor/lib/launcher.js');
//launcher.init(pathToConf+'html_runner_screenshot.js',{foo:'bar'})
var screenshotOptions = {
  envs: [{ name: 'oat', selected: true }, { name: 'sit' }],
  whitelabels: [{ name: 'dcexe.com', selected: true }, { name: 'anxbtc.com', selected: true }, { name: 'anxpro.com' }],
  pages: [{ name: '/pages/about', selected: true }, { name: '/pages/fees', selected: true }, { name: 'foo' }],
  sizes: [
    { name: 'laptop', x: 1140, y: 900, selected: false },
    { name: 'iPhone5', x: 320, y: 5000, selected: false },
    { name: 'S4', x: 1360, y: 5000, selected: true },
    { name: 'foo', x: 320, y: 5000, selected: false },
  ],
  filenames: [],
  resolve: undefined
};
var promise = new Promise(
  function(resolve, reject) { // (A)
    screenshotOptions.filenames = [];
    screenshotOptions.resolve = resolve;
    launcher.init(pathToConf + 'metaspec_screenshotter.js', { params: { screenshotOptions: screenshotOptions } });
  });
promise.then((fnames) => {
  console.log('resolved fnames', fnames);
});

console.log('fnames', screenshotOptions.filenames);
