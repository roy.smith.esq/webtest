#!/bin/bash
#
# Update the local jail contents using rsync from a central server, or ...
# if called with -u, do the opposite and update the server
SERVER=p3.primetext.com
USER=rrsync
JAILOWNER=anx
JAILNAME="webtest-jail"

# change into the jail directory
if [ -z "$WEBTEST_JAIL" ]
then
  echo You must 'export WEBTEST_JAIL=/the/path/of/your/webtest-jail'. Exiting
  exit 1
fi
cd $WEBTEST_JAIL


# if an upload request (Roy only)
if [ "$1" == "-u" ]
then
  # clean out tmp first
  sudo rm -rf tmp/* tmp/.*  2> /dev/null
  # run the rsync under sudo to upload changes
  sudo rsync  -azv --del \
    --exclude 'screenshots' --exclude 'dev/*' --exclude 'run' --exclude '*.log' --exclude='var/cache/' --exclude 'GITREPO' --exclude '.npm' --exclude 'proc' --exclude 'tmp' --exclude '*.zip' \
    -e "ssh -p12322" --protocol=29 . $USER@$SERVER:~$JAILOWNER/$JAILNAME
  exit $?
fi

cat << EOF
The following command is being run with sudo, so you will optionally be prompted for your local password.
You will then be prompted for the password of the rrsync user, which starts with 0 and ends with 1
EOF
sudo rsync  -avz --del \
     --exclude 'screenshots' --exclude 'dev/*' --exclude 'run' --exclude '*.log' --exclude='var/cache/' --exclude 'GITREPO' --exclude '.npm' --exclude 'proc' --exclude 'tmp' --exclude '*.zip' \
-e "ssh -p12322" --protocol=29 $USER@$SERVER:~$JAILOWNER/$JAILNAME/ .
