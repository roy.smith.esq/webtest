/// <reference path="./refs.d.ts"/>
declare var tags
export module Helpers {
  var fs = require('fs');

  export interface Params {
    env?: string,                                          // eg oat
    whitelabel?: string,                                   // eg anxbtc.com
    login?: { verification: string },                        // eg {verification: 'VERIFIED'}
    password?: string,
    baseUrl?: string,
    width?: number,
    height?: number,
  }

  /**
   * Allows a spec to define its Prerequisites.
   * env/whitelabel are used to override the command line values, eg for a spec that can only run on a specific whitelabel.
   * login is used to request that before the spec is run, the browser is logged on using an apporpriate account.
   * tags is an array of strings used to select this spec for inclusion in a run
   */
  export interface AnxUnitOptions {
    env?: string,
    whitelabel?: string
    tags: String[],
    login?: LoginOptions,
  }

  export interface LoginOptions {
    verification: string;
  }

  export var  // enum of verified email suffixes
    verification = {
      none: '',
      verified: 'VERIFIED',
      unverified: 'UNVERIFIED',
      preverified: 'PRE_VERIFIED',
    }

  export enum TwofaType {
    NONE,
    SMS,
    OTP
  }

  /*
  A collection of small helpers
   */
  export class MiscHelpers {
    SS_PATH = '/screenshots/';
    SCHEME = 'https://';
    params: Params = {};
    currentPath: string;
    currentLoginState: LoginOptions;
    anxUnitOptions: AnxUnitOptions;



    /**
     * the constructor parses any environment or passed parameters and process, exposes them
     * to the rest of the specs. Thus specs should NOT generally access process.env
     * but should instead look for their equivalents here. This is because it is this constructor
     * that deals with things like defaults and generated values.
     *
     *
     *
     * @method constructor
     * @return {[type]}    [description]
     */
    constructor() {
      this.log('in mh constructor')
      // parse command line
      try {
        if (!browser) {  // if not being called from a test run, do nothing
          return;
        }
      } catch (ex) {
        return;
      }
      this.log('command line params anx ', browser.params.anx);
      try {
        // convert JSON string from --params.anx="" command line to object
        browser.params.anx = JSON.parse(browser.params.anx);
      } catch (ex) {
        browser.params.anx = {};
      }
      if (browser.params.anx.env) {
        this.params.env = browser.params.anx.env;
      }
      if (browser.params.anx.whitelabel) {
        this.params.whitelabel = browser.params.anx.whitelabel;
      }
      if (browser.params.anx.login) {
        this.params.login = browser.params.anx.login;
      }

      // this.log('params', browser.params);
      // this.log('paramsa', browser.params.anx.foo);
      this.params.password = process.env.ANX_PASSWORD;
      this.params.baseUrl = process.env.ANX_URL;
      this.params.width = process.env.ANX_X ? process.env.ANX_X : 1200;
      this.params.height = process.env.ANX_Y ? process.env.ANX_Y : 700;
      this.log('height = ' + this.params.height);
      this.log('baseurl = ' + this.params.baseUrl);
      // browser.manage().window().setSize(320, 480);
      browser.manage().window().setSize(1 * this.params.width, 1 * this.params.height);
    }

    /**
     * Uses executeAsyncScript to grab a reference to the HttpService, store it on window
     * and then add a mocked response entry..
     *
     * NB this must only be called once a controller that is injected with HttpService has run
     * otherwise HttpService won't yet have been instantiated.
     *
     * @param  {[type]}     url          [description]
     * @param  {[type]}     httpResponse [description]
     */
    addMockedUrl(url, httpResponse):void {
      browser.executeAsyncScript(() => {
        var url = arguments[arguments.length - 3];
        var httpResponse = arguments[arguments.length - 2];
        var callback = arguments[arguments.length - 1];
        function loadHttpService() {
          try {
            window['HttpService'] = window['angular'].element(document.body).injector().get('HttpService')
          } catch (ex) {
            console.log('HS load failed. retrying...' + ex);
            setTimeout(function() { loadHttpService() }, 1000);
            return;
          }
          window['HttpService']._mm[url] = httpResponse;
          callback(url + ' mocked');
        }
        loadHttpService();
      }, url, httpResponse)
        .then((status) => {
        console.log('[mh130] cb status ', status);
      });
    }

    /**
     * return true if height > width
     * @method isOrietnationPortrait
     * @return {boolean}             [description]
     */
    isOrientationPortrait(): boolean {
      return (1 * this.params.height > 1 * this.params.width);
    }

    /**
     * set window height to 20,000 to allow screenshot to capture full page
     * @method setInfiniteHeight
     */
    setInfiniteHeight(): void {
      browser.manage().window().setSize(1 * this.params.width, 20000);
    }
    /**
     * takes a Screenshot into /screenshots/{filename}.png
     *
     * @method takeScreenShot
     * @param  {[type]}       filename [description]
     */
    takeScreenshot(filename): void {
      browser.takeScreenshot().then((png) => {
        var fname = this.SS_PATH + filename.replace('.png', '') + '.png';
        var stream = fs.createWriteStream(fname);
        stream.write(new Buffer(png, 'base64'));
        stream.end();
        this.log('[ss11] Saving screenshot ' + fname);
      })
    }

    /**
     * called at the start of a unit test spec to register the Prerequisites for that spec.
     * main purpose is to store the anxUnitOptions so they're available for stuff like login
     *
     * NB it will be called twice. Firstly it will be called by the tagExplorer as the spec is required. It will then
     * be called from protractor when the spec is executed.
     *
     * @param  {[type]}          module              [description]
     * @param  {[type]}          anxUnitOptions [description]
     */
    setAnxUnitOptions(module, anxUnitOptions: AnxUnitOptions): void {
      this.anxUnitOptions = anxUnitOptions;
      // if being called as part of spec discovery (as opposed to spec execute), export the options to the current require module
      if (module) {
        module.exports.anxOptions = anxUnitOptions;
      }
      // this.log('in setAnxUnitOptions with H = ', this.anxUnitOptions);
    }

    /**
     * does browser.get to load a page. The url is constructed from a combination of the path arg and site prefs from anxUnitOptions and
     * the protractor command line, unless the path is already a full url, eg when called from screenshotter.
     *
     * If required, will also login
     *
     * @param  {string}  pathOrUrl      [description]
     * @param  {boolean} loginDone [description]
     */
    pageLoad(pathOrUrl: string, promise: webdriver.promise.Promise<any>, loginDone?: boolean): void {
      this.log('in pageLoad with ax', this.anxUnitOptions);
      if (pathOrUrl.startsWith(this.SCHEME)) {
        var url = pathOrUrl;
      } else {
        var url = this.SCHEME + this.params.env + '.' + this.params.whitelabel + pathOrUrl
      }
      if (pathOrUrl === this.currentPath) {
        this.log(url + ' already requested, nothing to do.');
        return;
      }
      if (!loginDone && !this.objectsAreEqual(this.anxUnitOptions.login, this.currentLoginState)) {
        // if (!loginDone) {
        this.log('login needed', this.anxUnitOptions.login, this.currentLoginState)
        this.doLoginByXHR(url, this.anxUnitOptions, () => { this.pageLoad(pathOrUrl, promise, true) }).then((resp) => {
          // when the login XHR returns...
          this.log('resp', resp)
          this.currentLoginState = this.anxUnitOptions.login;
          // this.pageLoad(path);
        })
        return;
      }

      this.currentPath = pathOrUrl;

      this.log('getting ' + url);
      // fix to allow the naked domain to be loaded
      url = url.replace('naked.','');
      browser.get(url).then(() => {
        this.log(url + ' loaded');
        // browser.getCurrentUrl().then((u)=> {
        // browser.getLocationAbsUrl().then((u)=>{
        // console.log('url='+u,u);
        // })
      });
    }

    /**
     * Uses XHR to call the login endpoint, which logs in by setting various cookies. To satisfy lack of CORS on the endpoint
     * must first load a base page from the appropriate domain
     *
     * @param  {[type]}                             anxUnitOptions [description]
     * @param  {Function}                           cb             [description]
     * @return {protractor.promise.Promise<string>}                [description]
     */
    doLoginByXHR(url:string, anxUnitOptions, cb): protractor.promise.Promise<void> {
      var creds = this.getEmailAndPasswordForLogin(anxUnitOptions);
      // var url =
      //   this.SCHEME
      //   + (this.anxUnitOptions.env || this.params.env) + '.'
      //   + (this.anxUnitOptions.whitelabel || this.params.whitelabel);
      this.log('[mh219] getting page to allow login post ' + url)
      var adjustedUrl = url.replace('naked.','');
      var got = browser.get(adjustedUrl);
      got.then(() => {
        // --- start of JS that will execute on the browser
        var ret = browser.executeAsyncScript(function() {
          var url = arguments[arguments.length - 3];
          var creds = arguments[arguments.length - 2];
          console.log('calling xhr login with ' + url, creds)
          var callback = arguments[arguments.length - 1];
          var xhr = new XMLHttpRequest();
          var json = '{"username":"' + creds.e + '","password":"' + creds.p + '","trusted":true}';
          // setTimeout(function() {
          // hack to parse base URL from URL
          var parser = document.createElement('a');
          parser.href = url;
          url = parser.protocol + '//' + parser.host;
          if (parser.port.length > 0) {
            url += ':' + parser.port;
          }
          xhr.open('POST', '/moon/v1/login', true);
          xhr.setRequestHeader('Content-type', 'application/json');
          xhr.setRequestHeader('Content-length', '' + json.length);
          xhr.setRequestHeader('Connection', 'close');
          xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
              console.log('xhr4 ' + xhr.status, xhr.responseBody + ' ' + url);
              if (xhr.status > 299) {
                callback(xhr.status);
                alert('done. Login status ' + xhr.status + ' ' + creds.e + ' ' + creds.p + ' ' + url);
              }
              callback(xhr.status);
            }
          };
          xhr.send(json);
          // callback('debug')
          // }, 2000);
        }, url, creds);                                                         // args passed to remote script
        // --- end of JS that will execute on the browser
        ret.then((status) => {
          this.log('[mh243] login status ' + status);
          // browser.pause();
          cb();
        })
      })
      // browser.pause()
      // return ret;
      // .then(function(str) {
      // console.log(JSON.parse(str)['food']);
      // });
      return got;
    }

    /**
     * uses the login prefs (currently only verification) to generate an email address and password
     * password is always abcd1234
     *
     * @param  {AnxUnitOptions}            anxUnitOptions [description]
     * @return {e:string, p:string        [description]
     */
    getEmailAndPasswordForLogin(anxUnitOptions: AnxUnitOptions): { e: string, p: string } {
      // var e = "anxe2etest+_VeRiFiEd@anx.hk";
      var e = "roy.smith.esq+_VeRiFiEd@gmail.com";
      var p = 'abcd1234';
      console.log('[mh281]  !!!!  warning - using roy.smith.esq@gmail.com while test accounts are broken !!!!!!')
      e = e.replace('VeRiFiEd', anxUnitOptions.login.verification);
      console.log('e ' + e, anxUnitOptions)
      return { e: e, p: p };
    }

    /**
     * test for object equality based on recursing object properties. used to detect change of login params
     * @param  {any}           a [description]
     * @param  {any}           b [description]
     * @return {boolean}         [description]
     */
    objectsAreEqual(a: any, b: any): boolean {
      if (!a && !b) {                                                           // both undefined = equal
        return true;
      }
      if (a == b) {                                                             // same object
        return true;
      }
      try {
        return Object.keys(a).length === Object.keys(b).length &&
          Object.keys(a).reduce((acc, k) => acc && this.objectsAreEqual(a[k], b[k]), true);
      } catch (ex) {
        return false;
      }
    }
    /**
   * @name waitForUrlToChangeTo
   * @description Wait until the URL changes to match a provided regex
   * @param {RegExp} urlRegex wait until the URL changes to match this regex
   * @returns {!webdriver.promise.Promise} Promise
   */
    waitForUrlToChangeTo(urlRegex): PromiseLike<PromiseLike<any>> {
      var currentUrl;
      return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
        currentUrl = url;
      }
        ).then(function waitForUrlToChangeTo() {
        return browser.wait(function waitForUrlToChangeTo() {
          return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
            return urlRegex.test(url);
          });
        });
      }
        );
    }
    //
    // doLogin(options: { whitelabel?: string, verified: Verified, twofa: TwofaType }) {
    //   // console.log(options)
    //   console.log(this.params)
    //   switch (options.verified) {
    //     case Verified.FALSE:
    //       var email = this.params.email_unverified
    //       break;
    //     case Verified.TRUE:
    //       var email = this.params.email_verified
    //       break;
    //   }
    //   console.log(email)
    //   element(by.id('email')).sendKeys(email);
    //   element(by.id('password')).sendKeys(this.params.password);
    //   element(by.model('trusted')).click();
    //   element(by.id('submit')).click();
    //   this.takeScreenshot('login');
    //
    //   // browser.ignoreSynchronization = true;
    //   if (options.twofa == TwofaType.SMS) {
    //     this.log('sms 2fa')
    //     browser.wait(protractor.until.elementLocated(By.id('dialog_1')), 40000)
    //       .then((d1) => {
    //       d1.getLocation().then(pos => {
    //         this.log('[h108] SMS prompt pos', pos);
    //         this.takeScreenshot('dcexe_sms2fa-1')
    //       })
    //       d1.getSize().then(size => {
    //         this.log('[h108] SMS prompt size', size);
    //       });
    //     })
    //   }
    //   if (options.twofa == TwofaType.NONE) {
    //     this.waitForUrlToChangeTo(/.*\/user/);
    //     // Make sure the cookie is still set.
    //     browser.manage().getCookie('session.key').then((cookie) => {
    //       expect(cookie.value).toMatch('......*')
    //       browser.manage().getCookie('tradedCurrency').then((cookie) => {
    //         expect(cookie.value).toMatch('...');
    //         browser.ignoreSynchronization = false;
    //
    //         browser.wait(protractor.until.elementLocated(By.xpath('//*[@id="viewport"]/section/section/div/div/section/section[1]/div/article/a[2]')), 40000)
    //           .then(() => {
    //           this.log('[h102] fully logged in');
    //         })
    //
    //
    //       });
    //     });
    //   }
    //   // the line below is the "correct" technique, but times out due to anxstatic failing to quiesce
    //   // expect(browser.getCurrentUrl()).toEqual(BASE_URL+'/user');
    // }


    /**
     * facade for console log. Here to support advanced logging options in the future.
     * @method log
     * @param  {string} str [description]
     * @param  {any}    obj [description]
     */
    log(str: any, obj1?: any, obj2?: any): void {
      console.log(str, obj1 || '', obj2 || '');
    }
  }

  // instantiate and invoke constructor
  export var miscHelpers = new MiscHelpers();
}
