/// <reference path="./refs.d.ts"/>
export module Helpers {
  var fs = require('fs');

  export interface Params {
    email_unverified?: string,
    email_verified?: string,
    password?: string,
    baseUrl?: string,
    width?: number,
    height?: number,
  }

  export enum TwofaType {
    NONE,
    SMS,
    OTP
  }

  export enum Verified {
    FALSE,
    TRUE,
  }
  /*
  A collection of small helpers
   */
  export class MiscHelpers {
    SS_PATH = '/screenshots/';
    params: Params;

    /**
     * the constructor parses any environment or passed parameters and process, exposes them
     * to the rest of the specs. Thus specs should NOT genberally access process.env
     * but shold instead look for their equivalents here. This is because it is this constructor
     * that deals with things like defaults and generated values.
     *
     *
     *
     * @method constructor
     * @return {[type]}    [description]
     */
    constructor() {
      this.params = {};
      this.params.email_unverified = process.env.ANX_EMAIL_U;
      this.params.email_verified = process.env.ANX_EMAIL_V;
      this.params.password = process.env.ANX_PASSWORD;
      this.params.baseUrl = process.env.ANX_URL;
      this.params.width = process.env.ANX_X;
      this.params.height = process.env.ANX_Y;
      console.log('height = '+this.params.height)
      // browser.manage().window().setSize(320, 480);
      browser.manage().window().setSize(1 * this.params.width, 1 * this.params.height);
    }

    /**
     * takes a Screenshot into /screenshots/{filename}.png
     *
     * @method takeScreenShot
     * @param  {[type]}       filename [description]
     */
    takeScreenshot(filename): void {
      browser.takeScreenshot().then((png) => {
        var fname = this.SS_PATH + filename + '.png';
        var stream = fs.createWriteStream(fname);
        stream.write(new Buffer(png, 'base64'));
        stream.end();
        console.info('[ss11] Saving screenshot ' + fname);
      })
    }

    /**
   * @name waitForUrlToChangeTo
   * @description Wait until the URL changes to match a provided regex
   * @param {RegExp} urlRegex wait until the URL changes to match this regex
   * @returns {!webdriver.promise.Promise} Promise
   */
    waitForUrlToChangeTo(urlRegex): PromiseLike<PromiseLike<any>> {
      var currentUrl;
      return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
        currentUrl = url;
      }
        ).then(function waitForUrlToChangeTo() {
        return browser.wait(function waitForUrlToChangeTo() {
          return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
            return urlRegex.test(url);
          });
        });
      }
        );
    }

    doLogin(options: { whitelabel?: string, verified: Verified, twofa: TwofaType }) {
      // console.log(options)
      console.log(this.params)
      switch (options.verified) {
        case Verified.FALSE:
          var email = this.params.email_unverified
          break;
        case Verified.TRUE:
          var email = this.params.email_verified
          break;
      }
      console.log(email)
      element(by.id('email')).sendKeys(email);
      element(by.id('password')).sendKeys(this.params.password);
      element(by.model('trusted')).click();
      element(by.id('submit')).click();
      this.takeScreenshot('login');

      // browser.ignoreSynchronization = true;
      if (options.twofa == TwofaType.SMS) {
        this.log('sms 2fa')
        browser.wait(protractor.until.elementLocated(By.id('dialog_1')), 40000)
          .then((d1) => {
          d1.getLocation().then(pos => {
            this.log('[h108] SMS prompt pos', pos);
            this.takeScreenshot('dcexe_sms2fa-1')
          })
          d1.getSize().then(size => {
            this.log('[h108] SMS prompt size', size);
          });
        })
      }
      if (options.twofa == TwofaType.NONE) {
        this.waitForUrlToChangeTo(/.*\/user/);
        // Make sure the cookie is still set.
        browser.manage().getCookie('session.key').then((cookie) => {
          expect(cookie.value).toMatch('......*')
          browser.manage().getCookie('tradedCurrency').then((cookie) => {
            expect(cookie.value).toMatch('...');
            browser.ignoreSynchronization = false;

            browser.wait(protractor.until.elementLocated(By.xpath('//*[@id="viewport"]/section/section/div/div/section/section[1]/div/article/a[2]')), 40000)
              .then(() => {
              this.log('[h102] fully logged in');
            })


          });
        });
      }
      // the line below is the "correct" technique, but times out due to anxstatic failing to quiesce
      // expect(browser.getCurrentUrl()).toEqual(BASE_URL+'/user');
    }


    /**
     * facade for console log. Here to support advanced logging options in the future.
     * @method log
     * @param  {string} str [description]
     * @param  {any}    obj [description]
     */
    log(str: string, obj?: any): void {
      console.log(str, obj || '');
    }
  }
}
