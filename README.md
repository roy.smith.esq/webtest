# Introduction
The e2e test environment (e2ee) adds a layer above Angular protractor (which in turn is a layer above Selenium), which makes it easy to write unit tests for the anxstatic codebase.

Additionally, it provides a web service that can be used to request screenshots of selected pages/environments.

All of the files required to run e2ee are packaged in a downloadable chroot jail based on Ubuntu 15. This jail, along with the web service) is also installed on the ANX e2e server at 52.74.205.223 (e2es).

# About this repo
Primarily this repo is a container for unit tests and various helper methods. Unit tests should live within the protractor/spec/tagged/unit_tests folder (or a subfolder thereof).

However, Hugh requested that e2ee should also provide a web service that management users to request screenshots. Rather than create a second repo, all of the functionality associated with the screenshot service lives under `html_runner`. html-runner is a complete Angular 2, node express project, cloned from https://github.com/mgechev/angular2-seed . The original readme.md from this project is preserved in SEED_README.md.


# How to install the e2ee

The three steps required to get everything running are:-
1. Prepare your environment
2. Install the jail
3. Install the repo

## Prepare your environment
Decide where the repo and the jail will be installed, then add the following three lines to your `.bashrc` or equivalent
```
export WEBTEST_JAIL=~/jails/webtest-jail  # or wherever you intend to download and untar the webtest-jail download
export WEBTEST_REPO=/shared/gotrepos/anx/webtest # or wherever you intend to git clone the webtest repo
PATH=$PATH:$WEBTEST_REPO/scripts
```
... then run `. bash` to cause the .bashrc to be reread. Users of zsh should edit `.zshrc` and run `. zsh`


## Install the jail
Using wget or curl, download the jail tar and untar into the previously declared directory, eg.
```
cd $WEBTEST_JAIL/..
wget http://52.74.205.223/downloads/webtest-jail.tar.gz
sudo tar zxf webtest-jail.tar.gz
```

## Install the repo
```
mkdir -p $WEBTEST_REPO
cd $WEBTEST_REPO/..
git clone https://gitlab.com/roy.smith.esq/webtest.git
jail.sh npminstall
```
You can safely ignore the error messages around duplicate identifiers

## Testing everything is OK
You can run the screenshot server with
```
jail.sh serve
```
and then point your browser at http://localhost:5555/dist/dev/ (give it a few moments to build the app)

if the server fails to start, you can check its log in $WEBTEST_REPO/html_runner/nohup.out

# How to run a unit test
A typical call to run a unit test is
```
jail.sh -h runtags --tags=BAU-597 --params.anx='{"env":"oat","whitelabel":"anxbtc.com"}'
```
Where:-
* -h   
is an optional flag to indicate headless running. You would normally omit this so you can watch the test run.
* runtags   
 is the jail.sh sub-command to run tests based on a provided tag.
* --tags=BAU-597   
a comma separated list of tags. A tag is normally a BAU, or a category eg. registration
* --params.anx='{"env":"oat","whitelabel":"anxbtc.com"}'     
is a configuration object to specifiy which environment and which whitelabel to run against. NB This must not contain spaces.


# How to write a unit test
It's much easier than you might think. Firstly, make sure you have TypeScript installed since writing in TS is way simpler than writing in JS. Each unit test begins with a three lines of boilerplate, and is then a "describe" containing a number of "it"s. The best way to learn how to write unit tests is to study an existing one. I've marked up protractor/spec/tagged/unit_tests/BAU-648.ts with a bunch of comments, so this should serve as a good tutorial. Also look at protractor/spec/tagged/unit_tests/GOTHAM-49.ts, as this is an example of a unit test that requires a logged in session. Check the comment on line 3 to see that you simply need to declare the type of account in the call to setAnxUnitOptions and everything else is taken care of automagically.

You'll need to understand Jasmine for its describe/it structure and matchers, plus Selenium/protractor for the tools to fetch values from the page. This page https://angular.github.io/protractor/#/api contains pretty much everything you'll need to know.

# Helpers
The helper methods are defined in protractor/helpers/misc_helpers.ts. I won't reproduce the jsdoc comments here, go read them for yourself. The main methods are :-
setAnxUnitOptions, pageLoad, addMockedUrl and takeScreenshot.

# Built in tests
The file protractor/helpers/layout_helper.ts provides a method `runTests` which will look for special markup in the current page, and use that markup to validate the rendered HTML. It also looks for any elements with a `translate` attribute and confirms that its innerHTML doesn't look like an untranslated key.

## markup
You can annotate elements for automated testing by giving them an `x-test` attribute. The supported values of x-test are:-
### x-test="same('prop','offsetTop','sibling')"
Checks that this element is vertically aligned with an element with id='sibling'. "offsetTop" can be any element property.
### x-test="same('css','color','sibling')"
Checks that this element is the same color as the element with id='sibling'. "color" can be any CSS value.
### x-test="container('wrapper_div')"
Checks that this element is entirely contained within (ie doesn't overflow) an ancestor div with id="wrapper_div"

## Usage
To run the built in tests, simply include the following in your unit test.
```
import {LayoutHelper} from "../../helpers/layout_helper";
LayoutHelper.layoutHelper.runTests(current.filename);
```

# The screenshot server
You can run the screenshot server with
```
jail.sh {-h} serve
```
where -h is an optional "headless" flag. The server runs on localhost port 5555, can can be accessed at http://localhost:5555/dist/dev/, or more normally http://localhost:5555/dist/dev/#/screenshot


# AWS server

# NOTES ON login
e2ee was designed to run with pre-configured test accounts on all platforms. Last time I checked, this hadn't yet been completed, and so they are currently running using limited roy.smith.esq test accounts.
Check with Baron, and when the test accounts are configured correct the values for email and password in protractor/helpers/misc_helpers.ts line 316 and 317. Line 315 should be the correct value.
