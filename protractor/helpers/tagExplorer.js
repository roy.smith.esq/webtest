"use strict";
const misc_helpers_1 = require("./misc_helpers");
GLOBAL.describe = function (description, specDefinitions) { return; };
const fs_1 = require("fs");
var INITIALDIR = '/spec/tagged';
var PROTRACTOR_DIR = 'protractor/';
var INITIALDIR_FROM_ROOT = PROTRACTOR_DIR + INITIALDIR;
var INITIALDIR_FROM_THIS = '..' + INITIALDIR;
var CONFIG_FILE = PROTRACTOR_DIR + 'conf/tagged_specs.js';
var argTags = [];
var paramsArg;
var matchingSpecFilesMap = {};
var matchingSpecFilesArray = [];
function processArgs(argv) {
    argv.forEach((arg) => {
        processArg(arg);
    });
}
function processArg(arg) {
    if (!arg.startsWith('--')) {
        return;
    }
    if (arg.startsWith('--tags')) {
        var ts = arg.split('=')[1];
        argTags = ts.split(',');
    }
    if (arg.startsWith('--params')) {
        paramsArg = arg;
    }
}
function recurseDirectory(dirname) {
    console.log('Processing dir ', dirname);
    fs_1.readdirSync(dirname).forEach((name) => {
        let fullname = dirname + '/' + name;
        if (fs_1.statSync(fullname).isDirectory()) {
            return recurseDirectory(fullname);
        }
        else {
            processSpecFile(fullname);
        }
    });
}
function processSpecFile(fullname) {
    if (fullname.indexOf('.js') < 1) {
        return;
    }
    var fullnameRelativeToThis = fullname.replace(INITIALDIR_FROM_ROOT, INITIALDIR_FROM_THIS);
    try {
        var spec = require(fullnameRelativeToThis);
        if (!spec.anxOptions.login) {
            fullname;
            spec.anxOptions.login = { verification: misc_helpers_1.Helpers.verification.none };
        }
        if (spec.anxOptions && tagMatch(argTags, spec.anxOptions.tags)) {
            addMatchingSpecFile(fullname, spec.anxOptions.login.verification);
        }
    }
    catch (ex) {
        console.log('[te41] Exception scanning specfile ' + fullnameRelativeToThis, ex);
    }
}
function tagMatch(argTags, specTags) {
    for (var argTag of argTags) {
        for (var specTag of specTags) {
            if (argTag == specTag) {
                return true;
            }
        }
    }
    return false;
}
function addMatchingSpecFile(fullnameRelativeToThis, login) {
    let specFile;
    if (!matchingSpecFilesMap[login]) {
        specFile = { login: login, specFiles: [] };
        matchingSpecFilesMap[login] = specFile;
        matchingSpecFilesArray.push(specFile);
    }
    else {
        specFile = matchingSpecFilesMap[login];
    }
    specFile.specFiles.push(fullnameRelativeToThis);
    console.log('!!!', matchingSpecFilesMap, matchingSpecFilesArray);
}
function runProtractor(specFiles) {
    var specArg = '--specs=';
    for (var specFile of specFiles) {
        for (var filename of specFile.specFiles) {
            specArg += ',' + filename;
        }
    }
    specArg = specArg.replace('=,', '=');
    var cmd = 'protractor ' + specArg + ' \'' + paramsArg + '\' ' + CONFIG_FILE;
    console.log(cmd);
    var pro = require('child_process').spawn('protractor', [paramsArg, specArg, CONFIG_FILE]);
    pro.stdout.on('data', (data) => {
        console.log('stdout: ' + data);
    });
}
processArgs(process.argv);
recurseDirectory(INITIALDIR_FROM_ROOT);
runProtractor(matchingSpecFilesArray);
