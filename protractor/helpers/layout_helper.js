"use strict";
var LayoutHelper;
(function (LayoutHelper_1) {
    var fs = require('fs');
    class LayoutHelper {
        constructor() {
        }
        runTests(filename) {
            element.all(by.css('[x-test]'))
                .each((element, index) => {
                this.testElementForLayout(element, index, filename);
            });
            element.all(by.css('[translate]'))
                .each((element, index) => {
                this.testElementForUntranslatedString(element, index, filename);
            });
        }
        testElementForLayout(element, index, filename) {
            element.getAttribute('x-test').then((testMethod) => {
                var elementInfo = "x-test=" + testMethod + " as index " + index + " in " + filename;
                try {
                    let methodCall = 'this.' + testMethod.replace(')', ',element, elementInfo)');
                    console.log('m', methodCall);
                    eval(methodCall);
                }
                catch (ex) {
                    console.log(ex.toString);
                }
            });
        }
        ;
        testElementForUntranslatedString(element, index, filename) {
            element.getInnerHtml().then(ihtml => {
                expect(this.isTranslateKey(ihtml)).toBe(false, 'Because ' + ihtml + ' is an un-translated key in ' + filename);
            });
        }
        ;
        isTranslateKey(text) {
            if (!text)
                return false;
            if (new RegExp('[ ,:";\'-]').test(text))
                return false;
            if (text.length > 99)
                return false;
            if (text.endsWith('.'))
                return false;
            if (text.indexOf('.') < 0)
                return false;
            if (text.split('.').length > 1)
                return true;
            return false;
        }
        same(attributeType, attribute, someElement, thisElement, elementInfo) {
            console.log('Run Same helper.method %s %s %s ', attributeType, attribute, someElement);
            if (attributeType == 'css') {
                return this.sameCssValue(attribute, someElement, thisElement, elementInfo);
            }
            if (attributeType == 'prop') {
                return this.sameProp(attribute, someElement, thisElement, elementInfo);
            }
        }
        ;
        sameCssValue(attribute, someElementId, thisElement, elementInfo) {
            try {
                element(by.id(someElementId))
                    .getCssValue(attribute)
                    .then((someAttribute) => {
                    console.log('p=' + attribute);
                    console.log('sdp=' + someAttribute);
                    thisElement.getCssValue(attribute)
                        .then((thisAttribute) => {
                        console.log('ep=' + thisAttribute);
                        expect(thisAttribute).toBe(someAttribute, "Because element whose " + elementInfo + " where " + attribute + " position is not same as " + someElementId);
                    });
                }, (err) => {
                    console.log('[lh85] Error ' + err.name + ': ' + err.message + '. Check referenced ID');
                });
            }
            catch (ex) {
                console.log('ex', ex);
            }
        }
        sameProp(attribute, someElementId, thisElement, elementInfo) {
            try {
                element(by.id(someElementId))
                    .getAttribute(attribute)
                    .then((someAttribute) => {
                    console.log('sdp=' + someAttribute);
                    thisElement.getAttribute(attribute)
                        .then((thisAttribute) => {
                        console.log('ep=' + thisAttribute);
                        expect(thisAttribute).toBe(someAttribute, "Because element whose " + elementInfo + " where " + attribute + " position is not same as " + someElementId);
                    });
                }, (err) => {
                    console.log('[lh85] Error ' + err.name + ': ' + err.message + '. Check referenced ID');
                });
            }
            catch (ex) {
                console.log('ex', ex);
            }
        }
        container(containerElementId, thisElement, elementInfo) {
            console.log('Run Container method');
            var containerElement = element(by.id(containerElementId));
            this.check_container_size(containerElement, thisElement, containerElementId, elementInfo);
            this.check_container_overflow('width', containerElement, thisElement, containerElementId, elementInfo);
        }
        check_container_size(containerElement, thisElement, containerElementId, elementInfo) {
            containerElement
                .getSize()
                .then((containerDimensions) => {
                thisElement.getSize()
                    .then((elementDimensions) => {
                    expect(elementDimensions.width).toBeLessThan(containerDimensions.width, "Because element " + elementInfo + " whose width is larger than " + containerElementId);
                    expect(elementDimensions.height).toBeLessThan(containerDimensions.height, "Because element '+elementInfo+' whose height is larger than " + containerElementId);
                });
            });
        }
        ;
        check_container_overflow(axis, containerElement, thisElement, containerElementId, elementInfo) {
            if (axis == 'height') {
                var offset = 'offsetTop';
            }
            else {
                var offset = 'offsetLeft';
            }
            containerElement
                .getAttribute(offset)
                .then((some_div_offset_top) => {
                thisElement.getAttribute(offset)
                    .then((element_offset) => {
                    containerElement.getSize()
                        .then((containerDimensions) => {
                        thisElement.getSize()
                            .then((element_dimensions) => {
                            expect((element_dimensions.height + parseFloat(offset))).toBeLessThan(containerDimensions.height, "Because element whose " + elementInfo + " " + axis + " is overflow on " + containerElementId);
                        });
                    });
                });
            });
        }
    }
    LayoutHelper_1.LayoutHelper = LayoutHelper;
    LayoutHelper_1.layoutHelper = new LayoutHelper();
})(LayoutHelper = exports.LayoutHelper || (exports.LayoutHelper = {}));
