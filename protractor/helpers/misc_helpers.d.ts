/// <reference path="refs.d.ts" />
export declare module Helpers {
    interface Params {
        email?: string;
        password?: string;
        baseUrl?: string;
        width?: number;
        height?: number;
    }
    enum TwofaType {
        NONE = 0,
        SMS = 1,
        OTP = 2,
    }
    class MiscHelpers {
        SS_PATH: string;
        params: Params;
        /**
         * the constructor parses any environment or passed parameters and process, exposes them
         * to the rest of the specs. Thus specs should NOT genberally access process.env
         * but shold instead look for their equivalents here. This is because it is this constructor
         * that deals with things like defaults and generated values.
         *
         *
         *
         * @method constructor
         * @return {[type]}    [description]
         */
        constructor();
        /**
         * takes a Screenshot into /tmp/{filename}.png
         *
         * @method takeScreenShot
         * @param  {[type]}       filename [description]
         */
        takeScreenShot(filename: any): void;
        /**
       * @name waitForUrlToChangeTo
       * @description Wait until the URL changes to match a provided regex
       * @param {RegExp} urlRegex wait until the URL changes to match this regex
       * @returns {!webdriver.promise.Promise} Promise
       */
        waitForUrlToChangeTo(urlRegex: any): PromiseLike<PromiseLike<any>>;
        doLogin(options: {
            twofa: TwofaType;
        }): void;
        /**
         * facade for console log. Here to support advanced logging options in the future.
         * @method log
         * @param  {string} str [description]
         * @param  {any}    obj [description]
         */
        log(str: string, obj?: any): void;
    }
}
