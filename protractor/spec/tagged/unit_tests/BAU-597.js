"use strict";
const misc_helpers_1 = require("../../../helpers/misc_helpers");
misc_helpers_1.Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['BAU-597', 'registration'] });
describe('Fixed BAU-597', () => {
    it('should display a translated string instead of "altcoinx.RegisterCommand.password.password.strength"', (done) => {
        misc_helpers_1.Helpers.miscHelpers.pageLoad('/register', undefined);
        misc_helpers_1.Helpers.miscHelpers.addMockedUrl('/moon/v1/user/verifyRegistration/DUMMY', { data: { "hasAcceptedTermsConditions": true, "countrySelectRequired": false, "country": "phl", "passwordChangeRequired": false, "language": "en_US" }, status: 200 });
        browser.setLocation('register/resetPassword/DUMMY').then(() => {
            browser.wait(protractor.until.elementLocated(By.id('passwordId')), 5000)
                .then(inp => {
                inp.sendKeys('1234ABCD');
                element(by.id('password2Id')).sendKeys('abcd1234');
                element(by.id('reset_password')).click();
            });
            browser.wait(protractor.until.elementLocated(By.id('server_error')), 5000)
                .then(span => {
                expect(span.getInnerHtml()).toMatch(/....*/);
                expect(span.getInnerHtml()).not.toMatch('altcoinx');
                done();
            });
        });
    });
});
