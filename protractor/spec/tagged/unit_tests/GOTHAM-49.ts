import {Helpers} from "../../../helpers/misc_helpers";
// Note that the call to setAnxUnitOptions below also includes a login property
// This will be noticed by any call to Helpers.miscHelpers.pageLoad which will esnure that an appropriate
// user is logged in before bavigating to the requested page.
Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['GOTHAM-49'], login: { verification: Helpers.verification.unverified } })
/// <reference path="../../../helpers/refs.d.ts"/>

describe('Fixed GOTHAM-49, when switching between basic and advanced ', () => {
  browser.ignoreSynchronization = true;
  Helpers.miscHelpers.pageLoad('/user', undefined);

  it('should not display busy.jade ', () => {
    browser.wait(protractor.until.elementLocated(By.id('advanced')), 20000)
      .then(inp => {
      inp.click().then(() => {
        console.log('GOTHAM-49: This spec is impossible to test via protractor because the fault is only visible briefly while JS is executing, but all protractor functions are suspended until JS completes, by which time the page has been refreshed.');
        console.log('GOTHAM-49: Test by observation while the spec is running');
      })
    });
  });
});
