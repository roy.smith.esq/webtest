"use strict";
var Helpers;
(function (Helpers) {
    var fs = require('fs');
    (function (TwofaType) {
        TwofaType[TwofaType["NONE"] = 0] = "NONE";
        TwofaType[TwofaType["SMS"] = 1] = "SMS";
        TwofaType[TwofaType["OTP"] = 2] = "OTP";
    })(Helpers.TwofaType || (Helpers.TwofaType = {}));
    var TwofaType = Helpers.TwofaType;
    (function (Verified) {
        Verified[Verified["FALSE"] = 0] = "FALSE";
        Verified[Verified["TRUE"] = 1] = "TRUE";
    })(Helpers.Verified || (Helpers.Verified = {}));
    var Verified = Helpers.Verified;
    class MiscHelpers {
        constructor() {
            this.SS_PATH = '/screenshots/';
            this.params = {};
            this.params.email_unverified = process.env.ANX_EMAIL_U;
            this.params.email_verified = process.env.ANX_EMAIL_V;
            this.params.password = process.env.ANX_PASSWORD;
            this.params.baseUrl = process.env.ANX_URL;
            this.params.width = process.env.ANX_X;
            this.params.height = process.env.ANX_Y;
            console.log('height = ' + this.params.height);
            browser.manage().window().setSize(1 * this.params.width, 1 * this.params.height);
        }
        takeScreenshot(filename) {
            browser.takeScreenshot().then((png) => {
                var fname = this.SS_PATH + filename + '.png';
                var stream = fs.createWriteStream(fname);
                stream.write(new Buffer(png, 'base64'));
                stream.end();
                console.info('[ss11] Saving screenshot ' + fname);
            });
        }
        waitForUrlToChangeTo(urlRegex) {
            var currentUrl;
            return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
                currentUrl = url;
            }).then(function waitForUrlToChangeTo() {
                return browser.wait(function waitForUrlToChangeTo() {
                    return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
                        return urlRegex.test(url);
                    });
                });
            });
        }
        doLogin(options) {
            console.log(this.params);
            switch (options.verified) {
                case Verified.FALSE:
                    var email = this.params.email_unverified;
                    break;
                case Verified.TRUE:
                    var email = this.params.email_verified;
                    break;
            }
            console.log(email);
            element(by.id('email')).sendKeys(email);
            element(by.id('password')).sendKeys(this.params.password);
            element(by.model('trusted')).click();
            element(by.id('submit')).click();
            this.takeScreenshot('login');
            if (options.twofa == TwofaType.SMS) {
                this.log('sms 2fa');
                browser.wait(protractor.until.elementLocated(By.id('dialog_1')), 40000)
                    .then((d1) => {
                    d1.getLocation().then(pos => {
                        this.log('[h108] SMS prompt pos', pos);
                        this.takeScreenshot('dcexe_sms2fa-1');
                    });
                    d1.getSize().then(size => {
                        this.log('[h108] SMS prompt size', size);
                    });
                });
            }
            if (options.twofa == TwofaType.NONE) {
                this.waitForUrlToChangeTo(/.*\/user/);
                browser.manage().getCookie('session.key').then((cookie) => {
                    expect(cookie.value).toMatch('......*');
                    browser.manage().getCookie('tradedCurrency').then((cookie) => {
                        expect(cookie.value).toMatch('...');
                        browser.ignoreSynchronization = false;
                        browser.wait(protractor.until.elementLocated(By.xpath('//*[@id="viewport"]/section/section/div/div/section/section[1]/div/article/a[2]')), 40000)
                            .then(() => {
                            this.log('[h102] fully logged in');
                        });
                    });
                });
            }
        }
        log(str, obj) {
            console.log(str, obj || '');
        }
    }
    Helpers.MiscHelpers = MiscHelpers;
})(Helpers = exports.Helpers || (exports.Helpers = {}));
