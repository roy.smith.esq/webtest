# Introduction

This directory contains a web app (browser and node server), used to run aspects of the e2e test suite. It is purely optional and all features can be run
from the command line.

The README for the original seed project is in SEED_README.md

This app uses:-
* angular2
* typescript
* angular-material
* gulp
* node

### Directory Structure

The full structure is in SEED_README, but the key bits are:-
* ./app             - the Angular2 browser app
* ./tools/utils     - the node server
