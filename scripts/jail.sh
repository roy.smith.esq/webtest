#!/bin/bash
#
# Runner for all webtest jail related tasks
#
# jail.sh is run with a suub-command, eg jail.sh mount or jail.sh serve
# Some of these commands, eg mount, are run within this script
# Others, eg serve are run by invoking chroot with this script as the command. So in the
# case of jail.sh serve, this script will call chroot jail.sh _serve to do the necessary
#

# the chroot command to use, either the suid version in jail, or a sudo
CHROOT="$WEBTEST_JAIL/usr/sbin/chroot --userspec=protractor:root"
XPID=/tmp/xpid
#set -x
#CHROOT="sudo chroot "

usage() { echo "Usage: $0 [-h] cmd  [cmd options]" 1>&2; exit 1; }

miscSetup() {
  echo doing miscSetup...
  sudo chmod 1777 /dev/shm
  sudo chmod 1777 /run/shm
  sudo chmod +s $WEBTEST_JAIL/usr/sbin/chroot
  sudo cp /etc/resolv.conf $WEBTEST_JAIL/etc
}

# conditionally mount subdirs
_mount() {
  # mount repo and proc
  mount | grep $WEBTEST_JAIL/GITREPO || sudo mount --bind $WEBTEST_REPO  $WEBTEST_JAIL/GITREPO
  mount | grep $WEBTEST_JAIL/proc || sudo mount --bind  /proc $WEBTEST_JAIL/proc
  mount | grep $WEBTEST_JAIL/dev || (sudo mount --bind  /dev $WEBTEST_JAIL/dev ; miscSetup)
  mount | grep $WEBTEST_JAIL/dev/pts || sudo mount --bind  /dev/pts $WEBTEST_JAIL/dev/pts
  mount | grep $WEBTEST_JAIL/dev/shm || sudo mount --bind  /dev/shm $WEBTEST_JAIL/dev/shm
  mount | grep $WEBTEST_JAIL/run || sudo mount --bind  /run $WEBTEST_JAIL/run
  # todo make the following two line conditional
  #sudo chmod 1777 /dev/shm
  # setuid on the bundled chroot to allow invocation without sudo
  #sudo chmod +s $WEBTEST_JAIL/usr/sbin/chroot
}

# conditionally unmount subdirs
_umount() {
  # un mount repo and proc
  sudo killall Xvfb
  mount | grep $WEBTEST_JAIL/GITREPO && sudo umount   $WEBTEST_JAIL/GITREPO
  mount | grep $WEBTEST_JAIL/proc && sudo umount $WEBTEST_JAIL/proc
  mount | grep $WEBTEST_JAIL/dev/shm && sudo umount $WEBTEST_JAIL/dev/shm
  mount | grep $WEBTEST_JAIL/dev/pts && sudo umount $WEBTEST_JAIL/dev/pts
  mount | grep $WEBTEST_JAIL/dev && sudo umount $WEBTEST_JAIL/dev
  mount | grep $WEBTEST_JAIL/run && sudo umount $WEBTEST_JAIL/run
}

# alias for mis-spelling of umount
_unmount() {
  _umount
}

# takes a freshly cloned webtest repo and does the setup stuff
_npminstall() {
  _mount
  $CHROOT $WEBTEST_JAIL /GITREPO/scripts/jail.sh $h _npminstall
}

# starts the html_runner node server by calling this script with _serve
_serve() {
  _mount
  export HOME=/home/protractor
  $CHROOT $WEBTEST_JAIL /GITREPO/scripts/jail.sh $h _serve
}

# runs an arbitrary command after a mount
_run() {
  _mount
  export HOME=/home/protractor
  $CHROOT $WEBTEST_JAIL /GITREPO/scripts/jail.sh $h _run $*
}

# runs protractor after a mount
_protractor() {
  _mount
  export HOME=/home/protractor
  #set -x
  $CHROOT $WEBTEST_JAIL /GITREPO/scripts/jail.sh $h _protractor $*
}

# runs the tagexplorer which in turn runs protractor after a mount
# eg. jail.sh  runtags --tags=BAU-648 --params.anx='{"env":"oat","whitelabel":"anxpro.com"}'
_runtags() {
  _mount
  export HOME=/home/protractor
  #set -x
  $CHROOT $WEBTEST_JAIL /GITREPO/scripts/jail.sh $h _runtags $*
}

# gives a shell in the chroot
_shell() {
  export HOME=/home/protractor
  $CHROOT $WEBTEST_JAIL /bin/bash
}

# starts Xvfb
_xvfb() {
  # sudo chroot $WEBTEST_JAIL /etc/init.d/xvfb restart
  $CHROOT $WEBTEST_JAIL /GITREPO/scripts/jail.sh  _xvfb
  #sudo start-stop-daemon --start  --pidfile $XPID --make-pidfile --background --exec $WEBTEST_JAIL/usr/bin/Xvfb --  :10 -extension RANDR -noreset -ac -screen 10 1024x768x16
}


# all of the following __ (that's a double underscore) functions are called recursively from within a chroot

# the recursed sibling of _runtags
__runtags() {
  #set -x
  echo foo
  cd /GITREPO
  if [ -n "$h" ]
  then
    export DISPLAY=:10
  fi
  #node protractor/helpers/tagExplorer.js --tags=BAU-648 --params.anx='{"env":"oat","whitelabel":"anxpro.com"}'
  node protractor/helpers/tagExplorer.js $*
}

# the recursed sibling of _serve
__serve() {
  id
  #set -x
  cd /GITREPO/html_runner
  if [ -n "$h" ]
  then
    export DISPLAY=:10
  fi
  (sleep 5 ; echo Point your browser at localhost:5555/dist/dev)&
  nohup npm run start &
}

# the recursed sibling of _npminstall
__npminstall() {
  HOME=/home/protractor
  cd /GITREPO
  typings install
  tsc -p .
  cd /GITREPO/html_runner
  npm install
  typings install
  npm rebuild node-sass
  tsc -p .
}

# the recursed sibling of _run
__run() {
  #set -x
  cmd=$1
  shift
  $cmd $*
}

# the recursed sibling of _protractor
__protractor() {
  cd /GITREPO/protractor/conf
  if [ -n '$h' ]
  then
    export DISPLAY=:10
  fi
  #set -x
  protractor $*
}

# the recursed sibling of _xvfb
__xvfb() {
  XPID=/tmp/xpid
  start-stop-daemon --start  --pidfile $XPID --make-pidfile --background --exec /usr/bin/Xvfb --  :10 -extension RANDR -noreset -ac -screen 10 1024x768x16
}


# ----------------------- MAIN --------------------------
while getopts "h" o; do
    case "${o}" in
        h)
            h='-h'
            ps -efw | grep vfb | grep -v grep || _xvfb
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "$WEBTEST_JAIL" ]
then
  echo You must 'export WEBTEST_JAIL=/the/path/of/your/protractor_jail'. Exiting
  exit 1
fi
if [ -z "$WEBTEST_REPO" ]
then
  echo You must 'export WEBTEST_REPO=/the/path/of/your/anx/repo'. Exiting
  exit 1
fi

# get the command to be run
cmd=$1
shift
# and call the corresponding function
_$cmd $*
