import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
@Injectable()
export class ScreenshotOptions {
  sizes: Size[] = [
    { name: 'mobile', x: 350, y: 5000 },
    { name: 'S4', x: 360, y: 5000 },
    { name: 'laptop', x: 1140, y: 900, selected: true },
    { name: 'iPad 3', x: 2048, y: 1536 }
  ];

  envs: Env[] = [
    { name: 'naked', selected: true },
    { name: 'oat' },
    { name: 'sit' },
    { name: 'uit' },
    { name: 'dev' },
  ];

  logins = {
    verifications: [
      { name: 'none' },
      { name: 'VERIFIED', selected: true },
      { name: 'UNVERIFIED' },
      { name: 'PRE_VERIFIED' }
    ]
  };

  whitelabels: Whitelabel[] = [
    { name: 'anxbtc.com', selected: true },
    { name: 'anxpro.com' },
    { name: 'dcexe.com' },
    { name: 'digatrade.com' },
    { name: 'atenpay.com' },
    { name: 'go-exchange.com' },
    { name: 'justcoin.com' },
    { name: 'justcoin.com' },
    { name: 'nig-ex.com' },
    { name: 'mydicewallet.com' },
  ];

  pages: Page[] = [
    { name: '/activity', selected: true },
    { name: '/advanced' },
    { name: '/card' },
    { name: '/confirmations/:code' },
    { name: '/dashboard' },
    { name: '/funds' },
    { name: '/login' },
    { name: '/message/paymentFailed' },
    { name: '/message/paymentSuccess' },
    { name: '/register' },
    { name: '/register/forgotPassword' },
    { name: '/register/resetPassword' },
    { name: '/register/resetPassword/:code' },
    { name: '/register/verifyRegistration' },
    { name: '/register/verifyRegistration/:code' },
    { name: '/reports' },
    { name: '/settings' },
    { name: '/settings/apiKeys' },
    { name: '/settings/notifications' },
    { name: '/settings/password' },
    { name: '/settings/twoFactor' },
    { name: '/settings/verify' },
    { name: '/settings/waiveEmailConfirmation' },
    { name: '/simple' },
    { name: '/stories/announcements' },
    { name: '/stories/market' },
    { name: '/stories/press' },
    { name: '/trade' },
    { name: '/user' },
    { name: '/' },
    { name: '/pages/about' },
    { name: '/pages/advantage' },
    { name: '/pages/api' },
    { name: '/pages/buysell' },
    { name: '/pages/card' },
    { name: '/pages/card-compare' },
    { name: '/pages/card-terms-basic' },
    { name: '/pages/careers' },
    { name: '/pages/contact' },
    { name: '/pages/crypto' },
    { name: '/pages/disclaimer' },
    { name: '/pages/faq' },
    { name: '/pages/fees' },
    { name: '/pages/getting-started' },
    { name: '/pages/investors' },
    { name: '/pages/kyc' },
    { name: '/pages/privacy' },
    { name: '/pages/security' },
    { name: '/pages/store' },
    { name: '/pages/team' },
    { name: '/pages/terms' },
    { name: '/pages/vault' }
  ];

  screenshotOptions: IScreenshotOptions;
  filenameList: IFilename[];
  public selectedVerification = 'VERIFIED';
  public selectedEnv = 'naked';
  public selectedSize = 'laptop';
  public selectedWhitelabel = 'anxbtc.com';

  constructor(private http: Http) {
    this.screenshotOptions = {
      envs: [{ name: 'naked', selected: true }],
      availableEnvs: this.envs,
      whitelabels: this.whitelabels,
      pages: this.pages,
      sizes: [{ name: 'laptop', x: 1140, y: 900, selected: true }],
      availableSizes: this.sizes,
      login: {
        verification: 'VERIFIED'
      }
    };
  }

  runScreenshot(opts: IScreenshotOptions, cb: (any)) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post('/runscreenshot', JSON.stringify(opts), { headers: headers })
      .subscribe(
      data => {
        console.log('data', data.json()[0]);
        this.filenameList = data.json();
        cb(data.json());
      },
      err => console.warn('err', err),
      () => {
        console.log('Submission Complete. Tests will now run async on the server while the client polls for files.');
      }
      );
  }

  fetchImage(fname) {
    var imgUrl = '/screenshots/' + fname.filename;
    this.http.get(imgUrl)
      .subscribe(
      data => {
        console.log('done ' + fname.filename);
        fname.fetched = true;
        fname.url = '/screenshots/' + fname.filename;
      },
      err => {
        if (err.status !== 404) {
          console.warn('err', err);
        }
        setTimeout(() => {
          this.fetchImage(fname);
        }, 5000);
        return;
      },
      () => {
        console.log('Submission Complete. Tests will now run async on the server while the client polls for files.');
      }
      );
  }

  getFilenames(): IFilename[] {
    return this.filenameList;
  }
  getSizes(): Size[] {
    return this.sizes;
  }
  setSize(name: string, x: number, y: number) {
    this.screenshotOptions.sizes = [{ name: name, x: x, y: y, selected: true }];
  }
  setLogin(login: { verification: string }) {
    this.screenshotOptions.login = login;
  }
  getEnvs(): Env[] {
    return this.envs;
  }
  setEnv(name: string) {
    this.screenshotOptions.envs = [{ name: name, selected: true }];
  }
  getWhitelabels(): Whitelabel[] {
    return this.whitelabels;
  }
  getPages(): Page[] {
    return this.pages;
  }
  getOptions(): IScreenshotOptions {
    return this.screenshotOptions;
  }
  getLoginVerifications(): Verification[] {
    return this.logins.verifications;
  }

  addPage(name: string): void {
    this.pages.push({ name: name });
  }
  addEnv(name: string): void {
    this.envs.push({ name: name });
  }
  addWhitelabel(name: string): void {
    this.whitelabels.push({ name: name });
  }
  addSize(name: string, x: number, y: number): void {
    this.sizes.push({
      name: name,
      x: x,
      y: y
    });
  }
}
export interface Env {
  selected?: boolean;
  name: string;
}
export interface Whitelabel {
  selected?: boolean;
  name: string;
}
export interface Page {
  selected?: boolean;
  name: string;
}
export interface Verification {
  selected?: boolean;
  name: string;
}
export interface Size {
  selected?: boolean;
  name: string;
  x: number;
  y: number;
}

interface IScreenshotOptions {
  envs: Env[];                                      // single entry with the chosen env (chosen radio)
  availableEnvs: Env[];                             // all possible envs for (radio)
  whitelabels: Whitelabel[];
  pages: Page[];
  sizes: Size[];                                    // single entry with the chosen size (chosen radio)
  availableSizes: Size[];                           // all possible sizes (radio)
  login: {
    verification: string
  };
}

interface IFilename {
  url: string;
  filename: string;
  fetched: boolean;
}
