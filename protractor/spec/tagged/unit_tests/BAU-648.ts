// always start with these three lines and add whatever tags are appropriate
import {Helpers} from "../../../helpers/misc_helpers";
Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['BAU-648', 'registration'] })
/// <reference path="../../../helpers/refs.d.ts"/>

// tests are written in Jasmine 2, so will always be wrapped in a "describe" and one or more "it"
describe('Fixed BAU-648', () => {
  it('should display a translated string instead of "altcoinx.RegisterCommand.password.password.strength"', (done) => {
    // the stuff inside the "it" is specific to the test you want to write.
    // This is an interesting example as it shows how HttpService.ts can (and should!!!) be used to make
    // API REST calls in a way that can easily be mocked for testing.
    //
    // start by loading a page which is guaranteed to instantiate HttpService. This shows the simple use of the
    // pageLoad Helper. Assuming you are using a TypeScript-enabled IDE such as WebStorm or atom, you can However
    // your mouse over the helper methid name to see its documentation.
    Helpers.miscHelpers.pageLoad('/register',undefined);
      // then set up a mock response so when we load the password reset page, DUMMY is accepted as the registration code
    Helpers.miscHelpers.addMockedUrl(
      '/moon/v1/user/verifyRegistration/DUMMY',
      { data: { "hasAcceptedTermsConditions": true, "countrySelectRequired": false, "country": "phl", "passwordChangeRequired": false, "language": "en_US" }, status: 200 }
    )
    // });

    // now that the HttpService has been mocked, load the password reset page
    // browser.setLocation is the way to navigate within the app without doing a page reload.
    // In this case instead of a valid registration code, we can use DUMMY because our mocked http response
    // will generate a valid response.
    browser.setLocation('register/resetPassword/DUMMY').then(() => {
      // wait for the password field to be renderered
      browser.wait(protractor.until.elementLocated(By.id('passwordId')), 5000)
        .then(inp => {
        // enter the two short passwords
        inp.sendKeys('1');
        element(by.id('password2Id')).sendKeys('1');
        element(by.id('reset_password')).click();
      });

      // wait for the error message to be shown
      browser.wait(protractor.until.elementLocated(By.id('server_error')), 5000)
        .then(span => {
        // and check its contents
        expect(span.getInnerHtml()).toMatch(/....*/);               // regex at least 4 chars
        expect(span.getInnerHtml()).not.toMatch('altcoinx');        // and not the translate key
        // tell Jasmine that this it has been completed
        done();
      });
    });
  });
});
