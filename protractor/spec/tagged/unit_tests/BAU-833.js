"use strict";
const misc_helpers_1 = require("../../../helpers/misc_helpers");
misc_helpers_1.Helpers.miscHelpers.setAnxUnitOptions(module, { tags: ['BAU-833', 'fees'] });
describe('Fixed BAU-833', () => {
    it('should display a translated string instead of "fees.fundingType.CHEQUE"', (done) => {
        misc_helpers_1.Helpers.miscHelpers.pageLoad('/pages/fees', undefined);
        browser.sleep(10000)
            .then(inp => {
            browser.executeAsyncScript(() => {
                try {
                    var callback = arguments[arguments.length - 1];
                    var e = document.getElementById('t_ft');
                    e = [].concat(e)[0];
                    var s = window['angular'].element(e).scope();
                    console.log(s.data);
                    s.data.fiatDeposit.USD.CHEQUE = s.data.fiatDeposit.USD.BANK_WIRE;
                    s.$root.$digest();
                    callback('mocked' + JSON.stringify(s.data.fiatDeposit.USD));
                }
                catch (ex) {
                    callback(' exception ' + ex);
                }
            })
                .then((status) => {
                console.log('[BAU-833-22] cb status ', status);
                browser.sleep(1000).then(() => {
                    browser.wait(protractor.until.elementLocated(By.tagName('md-content')), 5000)
                        .then(content => {
                        content.getInnerHtml().then((ihtml) => {
                            expect(ihtml.indexOf('fees.fundingType')).toBe(-1);
                            done();
                        });
                    });
                });
            });
        });
    });
});
